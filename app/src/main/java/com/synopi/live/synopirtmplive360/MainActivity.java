package com.synopi.live.synopirtmplive360;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;
import com.synopi.live.synopirtmplive360.customexample.RtmpActivity;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;


public class MainActivity extends AppCompatActivity  implements View.OnClickListener, CountryCodePicker.OnCountryChangeListener {

    private static final String TAG = "MainActivity";
    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;

    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;

    private FirebaseAuth mAuth;

    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private ViewGroup mPhoneNumberViews;
    private ViewGroup mSignedInViews;

    private TextView mStatusText;
    private TextView mStatText;
    private TextView mDetailText;

    private EditText mPhoneNumberField;
    private EditText mServiceIdFieldText;
//    private TextView mPhoneNumberField;
//    private TextView mServiceIdFieldText;
    private TextView mServiceIdHeadingText;
    private EditText mVerificationField;

    private Button mStartButton;
    private Button mVerifyButton;
    private Button mResendButton;
    private Button mSignOutButton;
    private Button continueBtn;
    private Button mServiceIdOkBtn;
    private Button reVerifyBtn;

    private RelativeLayout clearLayBtn;
    private ImageView clearImgBtn;

    private ProgressBar progressBar;

    private String appUser;
    private String appServiceId;
    private String connection="0";
    private int verified =0;


    int serviceId_ok_clicked=0;

    private final static char[] hexArray = "0123456789abcdef".toCharArray();
    FirebaseUser mUser;
    Crypto crypto;
    String token="";
    String phone="";
    String rtmpUrl_main="";

    private CountryCodePicker cppSpinnerPhoneCode;
    private String countryPhoneCode="+880";
    private TextView phoneCodeText;

    boolean doubleBackToExitPressedOnce = false;
    String serviceIdFromTextField="";

    //for verify part
    String phone_number_for_verification="";
    private RelativeLayout numberKeyPad, numberCode;
    private EditText code_text_1, code_text_2, code_text_3, code_text_4, code_text_5, code_text_6;
    //private TextView code_text_1, code_text_2, code_text_3, code_text_4, code_text_5, code_text_6;
    public TextView text_1,text_2,text_3, text_4, text_5, text_6, text_7, text_8, text_9, text_0;
    public ImageView text_back, text_ok;
    private String s1="",s2="", s3="", s4="", s5="", s6="";

    //stored in shared preferences
    SharedPreferences sharedpreferences;
    public static final String myPrefere = "mypref";
    public static final String Service_Id = "serviseId";
    public static final String User_Name = "userName";
    public static final String Phone_Number = "phoneNumber";
    public static final String Continue_Button = "continueButton";

    InputMethodManager imm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedpreferences = this.getSharedPreferences(myPrefere, Context.MODE_PRIVATE);
        imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);

        checkPermission();
        getUsernameAlert();

        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //for verify part
        numberKeyPad=findViewById(R.id.numberKeyPadId);
        numberCode=findViewById(R.id.codeId);

        //for verify part-verify number field
        code_text_1 =findViewById(R.id.edt_id_1);
        code_text_2 =findViewById(R.id.edt_id_2);
        code_text_3 =findViewById(R.id.edt_id_3);
        code_text_4 =findViewById(R.id.edt_id_4);
        code_text_5 =findViewById(R.id.edt_id_5);
        code_text_6 =findViewById(R.id.edt_id_6);

        //for verify part-number keypad
        text_0=findViewById(R.id.number_0_id);
        text_1=findViewById(R.id.number_1_id);
        text_2=findViewById(R.id.number_2_id);
        text_3=findViewById(R.id.number_3_id);
        text_4=findViewById(R.id.number_4_id);
        text_5=findViewById(R.id.number_5_id);
        text_6=findViewById(R.id.number_6_id);
        text_7=findViewById(R.id.number_7_id);
        text_8=findViewById(R.id.number_8_id);
        text_9=findViewById(R.id.number_9_id);

        //for verify part- imageview
        text_back=findViewById(R.id.number_back_id);
        text_ok=findViewById(R.id.number_ok_id);

        // Assign views
        mPhoneNumberViews = (ViewGroup) findViewById(R.id.phone_auth_fields);
        mSignedInViews = (ViewGroup) findViewById(R.id.signed_in_buttons);

        //clear image icon for clear phone and verification number edittext field
        clearImgBtn=findViewById(R.id.clearImageBtnId);
        clearLayBtn=findViewById(R.id.clearImageLayId);

        //used for verification purpose
        mStatusText = (TextView) findViewById(R.id.status);
        mStatText = (TextView) findViewById(R.id.stat);
        mDetailText = (TextView) findViewById(R.id.detail);
        phoneCodeText = (TextView) findViewById(R.id.codeTextId);
        phoneCodeText.setText("+880");
        mDetailText.setText("");

        //Edit text for phone & verification code
        mPhoneNumberField =  findViewById(R.id.field_phone_number);
        mServiceIdFieldText =  findViewById(R.id.serviceId_textView_Id);
        mServiceIdHeadingText = (TextView) findViewById(R.id.serviceId_text_heading_Id);
        mVerificationField = (EditText) findViewById(R.id.field_verification_code);

        //Button for phone number verification
        mStartButton = (Button) findViewById(R.id.button_start_verification);
        mVerifyButton = (Button) findViewById(R.id.button_verify_phone);
        mResendButton = (Button) findViewById(R.id.button_resend);
        mSignOutButton = (Button) findViewById(R.id.sign_out_button);
        continueBtn= (Button) findViewById(R.id.continueBtnId);
        mServiceIdOkBtn= (Button) findViewById(R.id.button_serviceid_ok_id);
        reVerifyBtn= (Button) findViewById(R.id.reVerifyBtnId);
        cppSpinnerPhoneCode=findViewById(R.id.ccpSpinnerId);


        //for verify part- onclick
        text_0.setOnClickListener(this);
        text_1.setOnClickListener(this);
        text_2.setOnClickListener(this);
        text_3.setOnClickListener(this);
        text_4.setOnClickListener(this);
        text_5.setOnClickListener(this);
        text_6.setOnClickListener(this);
        text_7.setOnClickListener(this);
        text_8.setOnClickListener(this);
        text_9.setOnClickListener(this);
        text_ok.setOnClickListener(this);
        text_back.setOnClickListener(this);
        //text_back.setOnLongClickListener(this);

//        mPhoneNumberField.setOnClickListener(this);
//        mServiceIdFieldText.setOnClickListener(this);
//
//        mPhoneNumberField.setOnTouchListener(this);
//        mServiceIdFieldText.setOnTouchListener(this);

        // Assign click listeners
        mStartButton.setOnClickListener(this);
        mVerifyButton.setOnClickListener(this);
        mResendButton.setOnClickListener(this);
        mSignOutButton.setOnClickListener(this);
        continueBtn.setOnClickListener(this);
        clearLayBtn.setOnClickListener(this);
        clearImgBtn.setOnClickListener(this);
        mServiceIdOkBtn.setOnClickListener(this);
        reVerifyBtn.setOnClickListener(this);

        //country code spinner
        cppSpinnerPhoneCode.setOnCountryChangeListener(this);

        mPhoneNumberField.addTextChangedListener(textWatcher);
        code_text_6.addTextChangedListener(textWatcher);
        mServiceIdFieldText.addTextChangedListener(textWatcher);
        mVerificationField.addTextChangedListener(textWatcher);
        code_text_1.addTextChangedListener(textWatcher);
        code_text_2.addTextChangedListener(textWatcher);
        code_text_3.addTextChangedListener(textWatcher);
        code_text_4.addTextChangedListener(textWatcher);
        code_text_5.addTextChangedListener(textWatcher);
        code_text_6.addTextChangedListener(textWatcher);


        //Firebase instance
        mAuth = FirebaseAuth.getInstance();
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        //Firebase callback
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential);
                mVerificationInProgress = false;
                updateUI(STATE_VERIFY_SUCCESS, credential);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                viewVisibilityVisible(cppSpinnerPhoneCode,mPhoneNumberViews/*,numberKeyPad,phoneCodeText*/,mPhoneNumberField);

                Log.w(TAG, "onVerificationFailed", e);
                mVerificationInProgress = false;

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    mPhoneNumberField.setError("Verification failed.");
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.", Snackbar.LENGTH_SHORT).show();

                }

                updateUI(STATE_VERIFY_FAILED);
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);
                mVerificationId = verificationId;
                mResendToken = token;

                updateUI(STATE_CODE_SENT);
            }
        };
    }


    @Override
    protected void onResume() {
        //mAuth.signOut();
        super.onResume();
    }

    //checking the phone number is verified or not. if not then verify
    @Override
    public void onStart() {
        super.onStart();
        mDetailText.setText("Verification");
        viewVisibilityGone(mStatusText,numberKeyPad,phoneCodeText,mDetailText,mStatText,mPhoneNumberViews,mPhoneNumberField,cppSpinnerPhoneCode,mVerificationField,mStartButton,mVerifyButton,mResendButton,mServiceIdFieldText,mServiceIdHeadingText
        ,mServiceIdOkBtn,continueBtn,reVerifyBtn);
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
        verified = 1;

        if (mVerificationInProgress && validatePhoneNumber()) {
            Log.d(TAG, "onStart: 3");
            verified = 0;
            startPhoneNumberVerification(countryPhoneCode + mPhoneNumberField.getText().toString());
            mDetailText.setText("");
        }
        Log.d(TAG, "onStart: 4");
    }

        //Save continue button clicked number
    public void saveContinueClicked(String continue_clicked) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Continue_Button, continue_clicked);
        editor.commit();
    }

    //Retrieving phone number
    public String getContinueClicked() {
        String continue_clicked="";
        sharedpreferences = getSharedPreferences(myPrefere, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(Continue_Button)) {
            continue_clicked=sharedpreferences.getString(Continue_Button, "");
        }
        return continue_clicked;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
    }

    //starting of phone number verification
    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber, /*time duration*/90, TimeUnit.SECONDS, this, mCallbacks);

        mVerificationInProgress = true;
    }

    //Verify phone number with code
    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    //resend verification code for sign in with phone number verification
    private void resendVerificationCode(String phoneNumber, PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, 90, TimeUnit.SECONDS, this, mCallbacks, token);
    }

    //sign in with phone number verification
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        //mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();

                            //finding token
                            token = FirebaseInstanceId.getInstance().getToken();
                            Log.d(TAG, "onComplete: token_: "+token);
                            crypto=new Crypto(token);
                            //finding Currentuser
                            FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
                            phone=mUser.getPhoneNumber();

                            Log.d(TAG, "onComplete: token_ user: "+phone);
                            updateUI(STATE_SIGNIN_SUCCESS, user);
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                //mVerificationField.setError("Invalid code.");
                            }

                            updateUI(STATE_SIGNIN_FAILED);
                        }
                    }
                });
    }

    //sign out from phone number verification
    private void signOut() {
        Log.d(TAG, "signOut: is called");
        mAuth.signOut();
        //updateUI(STATE_INITIALIZED);
    }

    private void updateUI(int uiState) {
        updateUI(uiState, mAuth.getCurrentUser(), null);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            updateUI(STATE_SIGNIN_SUCCESS, user);
        } else {
            updateUI(STATE_INITIALIZED);
        }
    }

    private void updateUI(int uiState, FirebaseUser user) {
        updateUI(uiState, user, null);
    }

    private void updateUI(int uiState, PhoneAuthCredential cred) {
        updateUI(uiState, null, cred);
    }

    private void updateUI(int uiState, FirebaseUser user, PhoneAuthCredential cred) {
        switch (uiState) {

            case STATE_INITIALIZED:
                mDetailText.setText(null);
                break;

            case STATE_CODE_SENT:
                viewVisibilityGone(numberKeyPad,mPhoneNumberViews,mPhoneNumberField,phoneCodeText,cppSpinnerPhoneCode,mStartButton,mServiceIdFieldText,mServiceIdHeadingText
                        ,mServiceIdOkBtn,continueBtn,reVerifyBtn);
                viewVisibilityVisible(mDetailText,mStatText,mStatusText,mVerifyButton,mResendButton, mVerificationField);
                mDetailText.setText("Code sent");

                code_text_1.setText("");
                code_text_2.setText("");
                code_text_3.setText("");
                code_text_4.setText("");
                code_text_5.setText("");
                code_text_6.setText("");
                mServiceIdFieldText.setText("");
                serviceIdFromTextField="";
                mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
                viewVisibilityInvisible(progressBar);
                text_ok.setImageResource(R.drawable.icon_ok_gray);
                break;

            case STATE_VERIFY_FAILED:
                viewVisibilityVisible(mStartButton,mPhoneNumberField,mDetailText,mStatText,mStatusText,cppSpinnerPhoneCode,mPhoneNumberViews);
                viewVisibilityGone(numberKeyPad,phoneCodeText,mVerificationField,mVerifyButton,mResendButton,mServiceIdFieldText,mServiceIdHeadingText
                        ,mServiceIdOkBtn,continueBtn,reVerifyBtn);
                viewVisibilityInvisible(progressBar);
                mDetailText.setText("Invalid phone number or no internet");
                mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_red));
                break;

            case STATE_VERIFY_SUCCESS:
                viewVisibilityGone(numberKeyPad,phoneCodeText,mPhoneNumberViews,mPhoneNumberField,cppSpinnerPhoneCode,mVerificationField,mStartButton,mVerifyButton,mResendButton
                        ,continueBtn,reVerifyBtn);
                viewVisibilityVisible(mDetailText,mServiceIdOkBtn,mServiceIdHeadingText,mServiceIdFieldText,mStatText);
                viewVisibilityInvisible(progressBar,mStatusText);
                mStatText.setText("Synopi Live 360");
                mDetailText.setText("Verification successful");
                mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
                // Set the verification text based on the credential
                Log.d(TAG, "updateUI: serviceId_STATE_VERIFY_SUCCESS");
                if (cred != null) {
                    if (cred.getSmsCode() != null) {

                        //mVerificationField.setText(cred.getSmsCode());
                    } else {

                        mDetailText.setText("Verification successful Instantly");
                        mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
                    }
                }
                break;

            case STATE_SIGNIN_FAILED:
                //nothing was here
                viewVisibilityGone(numberKeyPad,mPhoneNumberViews,mPhoneNumberField,phoneCodeText,cppSpinnerPhoneCode,mStartButton,mServiceIdFieldText,mServiceIdHeadingText
                        ,mServiceIdOkBtn,continueBtn,reVerifyBtn);
                viewVisibilityVisible(mDetailText,mStatText,mStatusText,mVerifyButton,mResendButton, mVerificationField);

                mDetailText.setText("Enter valid code & click verify");
                mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_red));
                viewVisibilityInvisible(progressBar);
                break;

            case STATE_SIGNIN_SUCCESS:
                viewVisibilityGone(numberKeyPad,mPhoneNumberViews,mPhoneNumberField,phoneCodeText,cppSpinnerPhoneCode,mStartButton
                        ,continueBtn,reVerifyBtn,clearImgBtn,clearLayBtn,mVerificationField);
                viewVisibilityInvisible(mStatusText,progressBar);
                mDetailText.setText("Successfully verified your phone");
                text_ok.setImageResource(R.drawable.icon_ok_gray);
                Log.d(TAG, "updateUI: serviceId_STATE_SIGNIN_SUCCESS");
                mStatText.setText("Synopi Live 360");
                viewVisibilityVisible(mDetailText,mStatText,mServiceIdOkBtn,mServiceIdHeadingText,mServiceIdFieldText);
                //serviceId();
                break;
        }

        if (user == null) {
            viewVisibilityVisible(mPhoneNumberViews);
            //viewVisibilityGone(mSignedInViews);
        } else {
           // Signed in
           viewVisibilityGone(mPhoneNumberViews);
           if(isConnected()){
               connection="1";
           }
           if(verified ==1){
               //viewVisibilityGone(numberKeyPad);
           }else{
               if(getContinueClicked().equals("clicked")) {
                   Intent intent = new Intent(MainActivity.this, RtmpActivity.class);
                   intent.putExtra("continueBtnId", "");
                   intent.putExtra("connected", connection);
                   startActivity(intent);
                   finish();
               }else{
                   viewVisibilityGone(numberKeyPad,mPhoneNumberViews,mPhoneNumberField,phoneCodeText,cppSpinnerPhoneCode,mStartButton,mServiceIdFieldText,mServiceIdHeadingText
                           ,mServiceIdOkBtn,continueBtn,reVerifyBtn);
                   viewVisibilityVisible(mDetailText,mStatText,mStatusText,mVerifyButton,mResendButton, mVerificationField);
                   mDetailText.setText("Code sent");
                   mVerificationField.setText("");
                   code_text_1.setText("");
                   code_text_2.setText("");
                   code_text_3.setText("");
                   code_text_4.setText("");
                   code_text_5.setText("");
                   code_text_6.setText("");
                   mServiceIdFieldText.setText("");
                   serviceIdFromTextField="";
                   mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
                   viewVisibilityInvisible(progressBar);
                   text_ok.setImageResource(R.drawable.icon_ok_gray);
                   mAuth.signOut();
                   //Toast.makeText(MainActivity.this, "re verification", Toast.LENGTH_SHORT).show();
               }
           }
        }
    }

    @Override
    protected void onDestroy() {
        //mAuth.signOut();
        Log.d(TAG, "onDestroy: called");
        super.onDestroy();
    }

//    //Alert dialog for getting value of service id or shortcode
//    private void serviceId() {
//        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
//
//        LayoutInflater layoutInflater = this.getLayoutInflater();
//        View view = layoutInflater.inflate(R.layout.serviceid_alert,null);
//        //setting font
//        // calligrapher.setFont(view,"raleway_regular.ttf");
//        builder.setView(view);
//        final android.app.AlertDialog dialog = builder.create();
//        dialog.setCancelable(false);
//
//        final Button dialogButton = (Button) view.findViewById(R.id.serviceid_ok_id);
//        final EditText serviceIdText=view.findViewById(R.id.serviceid_id);
//        final ImageView cancelImageView= (ImageView) view.findViewById(R.id.btn_clear_keyId);
//        final RelativeLayout cancelImageView_key= (RelativeLayout) view.findViewById(R.id.btn_clear_lay_keyId);
//
//        cancelImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                serviceIdText.setText("");
//            }
//        });
//
//        cancelImageView_key.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                serviceIdText.setText("");
//            }
//        });
//
//        viewClickableTrue(dialogButton);
//
//        serviceIdText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if(serviceIdText.getText().toString().trim().length()>0) {
//                    dialogButton.setTextColor(getResources().getColor(R.color.white));
//                    dialogButton.setBackground(getDrawable(R.drawable.custom_button_aoo_color));
//                    viewVisibilityVisible(cancelImageView);
//                    viewClickableTrue(dialogButton);
//                }else{
//                    dialogButton.setTextColor(getResources().getColor(R.color.button_ok_color));
//                    dialogButton.setBackground(getDrawable(R.drawable.custom_button));
//                    viewVisibilityInvisible(cancelImageView);
//                    viewClickableFalse(dialogButton);
//                }
//            }
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//        // if ok button is clicked, close the custom dialog
//        dialogButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                appServiceId=serviceIdText.getText().toString();
//                crypto = new Crypto(token);
//                startHttpApiRequest();
//                mDetailText.setText("Successfully Verified your phone, click continue");
//                mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
//                viewVisibilityVisible(continueBtn);
//                //saveServiceId(appServiceId);
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();
//    }

    //Alert dialog for getting username
    private void getUsernameAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.username_alert,null);
        //setting font
        // calligrapher.setFont(view,"raleway_regular.ttf");
        builder.setView(view);
        final android.app.AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.alert_dialog_back));

        final Button dialogButton = (Button) view.findViewById(R.id.username_ok_id);
        final EditText username=view.findViewById(R.id.username_id);
        final ImageView cancelImageView= (ImageView) view.findViewById(R.id.btn_clear_keyId);
        final RelativeLayout cancelImageView_key= (RelativeLayout) view.findViewById(R.id.btn_clear_lay_keyId);

        cancelImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username.setText("");
            }
        });

        cancelImageView_key.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username.setText("");
            }
        });

        viewClickableTrue(dialogButton);

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(username.getText().toString().trim().length()>0) {
                    dialogButton.setBackground(getDrawable(R.drawable.custom_button_aoo_color));
                    dialogButton.setTextColor(getResources().getColor(R.color.white));
                    viewVisibilityVisible(cancelImageView);
                    viewClickableTrue(dialogButton);
                }else{
                    dialogButton.setBackground(getDrawable(R.drawable.custom_button));
                    dialogButton.setTextColor(getResources().getColor(R.color.button_ok_color));
                    viewVisibilityInvisible(cancelImageView);
                    viewClickableFalse(dialogButton);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        // if ok button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appUser=username.getText().toString();
                if(appUser.equals("")){

                }else {
                    mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
                    mDetailText.setText("Verification");
                    viewVisibilityGone(numberKeyPad,mVerificationField,mVerifyButton,mResendButton,mServiceIdFieldText,mServiceIdHeadingText
                            ,mServiceIdOkBtn,continueBtn,reVerifyBtn);
                    viewVisibilityVisible(mPhoneNumberViews,mStartButton,mPhoneNumberField/*,phoneCodeText*/,mStatusText,mStatText,mDetailText,cppSpinnerPhoneCode/*,numberKeyPad*/);
                    //serviceId_ok_clicked=1;
                    //saveUserName(appUser);
                    dialog.dismiss();
                }

            }
        });

        dialog.show();
    }

    //validating of phone number
    private boolean validatePhoneNumber() {
        String phoneNumber = mPhoneNumberField.getText().toString();
        Log.d(TAG, "validatePhoneNumber: "+phoneNumber);
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.setError("Invalid phone number.");
            return false;
        }
        return true;
    }

    //used for visible of views
    private void viewVisibilityVisible(View... views) {
        for (View v : views) {
            v.setVisibility(View.VISIBLE);
        }
    }

    //used for invisible of views
    private void viewVisibilityInvisible(View... views) {
        for (View v : views) {
            v.setVisibility(View.INVISIBLE);
        }
    }

    //used for visibility gone of views
    private void viewVisibilityGone(View... views) {
        for (View v : views) {
            v.setVisibility(View.GONE);
        }
    }

    //used for clickable true of views
    private void viewClickableTrue(View... views) {
        for (View v : views) {
            v.setClickable(true);
        }
    }

    //used for clickable false of views
    private void viewClickableFalse(View... views) {
        for (View v : views) {
            v.setClickable(false);
        }
    }


    //onclicking event fire
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.button_start_verification:
                if (!validatePhoneNumber()) {
                    return;
                }

                viewVisibilityVisible(progressBar);
                mDetailText.setText("Verifying...");
                mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
                startPhoneNumberVerification(countryPhoneCode+mPhoneNumberField.getText().toString());
                //savePhoneNumber(countryPhoneCode+mPhoneNumberField.getText().toString());
                break;

            case R.id.button_verify_phone:

                //String code = verificationCodeString();
                String code= mVerificationField.getText().toString();
                viewVisibilityVisible(progressBar);
                verifyPhoneNumberWithCode(mVerificationId, code);
                mDetailText.setText("Verifying...");
                mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
                break;

            case R.id.button_resend:
                mDetailText.setText("Re sending code...");
                mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
                mVerificationField.setText("");
                viewVisibilityVisible(progressBar);
                resendVerificationCode(countryPhoneCode+mPhoneNumberField.getText().toString(), mResendToken);
                break;

            case R.id.sign_out_button:
                signOut();
                break;

            case R.id.continueBtnId:
                if(rtmpUrl_main.equals("")|| rtmpUrl_main.toLowerCase().contains("undefine")
                        ||rtmpUrl_main.toLowerCase().contains("null")){
                    viewVisibilityInvisible(mStatusText);
                    viewVisibilityGone(numberKeyPad,phoneCodeText,mPhoneNumberViews,mPhoneNumberField,cppSpinnerPhoneCode,mVerificationField,mStartButton,mVerifyButton,mResendButton,mServiceIdFieldText,mServiceIdHeadingText
                            ,mServiceIdOkBtn,continueBtn);
                    viewVisibilityVisible(continueBtn,reVerifyBtn,mDetailText,mStatText);
                    mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_red));
                    mDetailText.setText("Phone number or service ID mismatch, Please verify with valid phone number & service ID");
                    //Toast.makeText(this, "This phone number is not on system, please verified with valid phone number", Toast.LENGTH_LONG).show();
                }else {
                    saveContinueClicked("clicked");
                    Intent intent = new Intent(MainActivity.this, RtmpActivity.class);
                    intent.putExtra("cameraAngle","Portrait");
                    intent.putExtra("connected", connection);
                    intent.putExtra("username", appUser);
                    intent.putExtra("continueBtnId", "continueBtnId");
                    intent.putExtra("serviceId", appServiceId);
                    intent.putExtra("rtmpLink", rtmpUrl_main);
                    intent.putExtra("phoneNumber", countryPhoneCode+mPhoneNumberField.getText().toString());
                    startActivity(intent);
                }
                break;
            case R.id.reVerifyBtnId:
                mVerificationField.setText("");
                mAuth.signOut();
                mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
                mDetailText.setText("Re verification");
                viewVisibilityGone(numberKeyPad,phoneCodeText,mVerificationField,mVerifyButton,mResendButton,mServiceIdFieldText,mServiceIdHeadingText
                        ,mServiceIdOkBtn,continueBtn,reVerifyBtn);
                viewVisibilityVisible(mPhoneNumberViews,mStartButton,mPhoneNumberField,cppSpinnerPhoneCode,mDetailText,mStatusText,mStatText);
                break;
            case R.id.clearImageBtnId:
                if (mPhoneNumberField.getVisibility()==View.VISIBLE){
                    mPhoneNumberField.setText("");
                    mStartButton.setBackground(getDrawable(R.drawable.custom_button));
                    viewVisibilityGone(clearImgBtn,clearLayBtn);
                }

                else if (numberCode.getVisibility()==View.VISIBLE){
                    //mVerificationField.setText("");
                    mVerifyButton.setBackground(getDrawable(R.drawable.custom_button));
                    viewVisibilityGone(clearImgBtn,clearLayBtn);
                }
                break;

            case R.id.clearImageLayId:
                if (mPhoneNumberField.getVisibility()==View.VISIBLE){
                    mPhoneNumberField.setText("");
                    mStartButton.setBackground(getDrawable(R.drawable.custom_button));
                    viewVisibilityGone(clearImgBtn,clearLayBtn);
                }
                break;

            case R.id.button_serviceid_ok_id:
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                appServiceId=mServiceIdFieldText.getText().toString();
                crypto = new Crypto(token);
                startHttpApiRequest();
                mDetailText.setText("Successfully verified your phone, Click continue");
                mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
                viewVisibilityInvisible(mStatusText);
                viewVisibilityGone(numberKeyPad,phoneCodeText,mPhoneNumberViews,mPhoneNumberField,cppSpinnerPhoneCode,mVerificationField,mStartButton,mVerifyButton,mResendButton,mServiceIdFieldText,mServiceIdHeadingText
                        ,mServiceIdOkBtn,reVerifyBtn);
//                viewVisibilityVisible(progressBar,mDetailText,mStatText);
//                mDetailText.setText("Loading...");
//                try {
//                    Thread.sleep(3000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }finally {
//                    viewVisibilityInvisible(progressBar,mDetailText);
                    viewVisibilityVisible(continueBtn,mDetailText,mStatText);
//                }

//                progressBarStatus = 0;
//                timeCount = 0;
//                new Thread(new Runnable() {
//                    public void run() {
//                        while (progressBarStatus < 100) {
//                            progressBarStatus = doSomeTasks();
//                            try {
//                                Thread.sleep(1000);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//
//                            // Update the progress bar
//                            progressBarHandler.post(new Runnable() {
//                                public void run() {
//                                    progressBar.setVisibility(View.VISIBLE);
//                                    progressBar.setProgress(progressBarStatus);
//                                }
//                            });
//                        }
//
//                        if (progressBarStatus >= 100) {
//                            try {
//                                Thread.sleep(2000);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//                            progressBar.setVisibility(View.INVISIBLE);
//                            viewVisibilityVisible(continueBtn);
//                        }
//                    }
//                }).start();


                break;

            case R.id.number_0_id:
                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    phoneEditTextValue("0");
                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    serviceIdTextValue("0");
                }else {
                    clickedValue("0");
                }
                break;

            case R.id.number_1_id:
                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    phoneEditTextValue("1");
                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    serviceIdTextValue("1");
                }else{
                    clickedValue("1");
                }
                break;

            case R.id.number_2_id:
                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    phoneEditTextValue("2");
                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    serviceIdTextValue("2");
                }else {
                    clickedValue("2");
                }
                break;

            case R.id.number_3_id:
                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    phoneEditTextValue("3");
                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    serviceIdTextValue("3");
                }else {
                    clickedValue("3");
                }
                break;

            case R.id.number_4_id:
                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    phoneEditTextValue("4");
                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    serviceIdTextValue("4");
                }else {
                    clickedValue("4");
                }
                break;

            case R.id.number_5_id:
                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    phoneEditTextValue("5");
                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    serviceIdTextValue("5");
                }else {
                    clickedValue("5");
                }
                break;

            case R.id.number_6_id:
                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    phoneEditTextValue("6");
                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    serviceIdTextValue("6");
                }else {
                    clickedValue("6");
                }
                break;

            case R.id.number_7_id:
                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    phoneEditTextValue("7");
                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    serviceIdTextValue("7");
                }else {
                    clickedValue("7");
                }
                break;

            case R.id.number_8_id:
                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    phoneEditTextValue("8");
                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    serviceIdTextValue("8");
                }else {
                    clickedValue("8");
                }
                break;

            case R.id.number_9_id:
                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    phoneEditTextValue("9");
                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    serviceIdTextValue("9");
                }else {
                    clickedValue("9");
                }
                break;

            case R.id.number_back_id:
                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    phoneEditTextValueDeleteValue();
                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    serviceIdDeleteValue();
                }else {
                    deleteValue();
                }

                break;

            case R.id.number_ok_id:

                if(mPhoneNumberField.getVisibility()==View.VISIBLE){
                    if (!validatePhoneNumber()) {
                        return;
                    }

                    viewVisibilityVisible(progressBar);
                    startPhoneNumberVerification(countryPhoneCode+mPhoneNumberField.getText().toString());

                }else if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                    viewVisibilityVisible(progressBar);
                    serviceIdOkValue();
                }else {
                    viewVisibilityVisible(progressBar);
                    verifyPhoneNumberWithCode(mVerificationId, verificationCodeString());
                }
                break;
        }
    }

    private void phoneEditTextValueDeleteValue() {
        String phonString=mPhoneNumberField.getText().toString();
        if(phonString.length()>0) {
            phone_number_for_verification = phonString.substring(0, phonString.length() - 1);
            mPhoneNumberField.setText(phone_number_for_verification);
        }else {
            mPhoneNumberField.setText("");
            phone_number_for_verification="";
        }
    }

    private void phoneEditTextValue(String value) {
        phone_number_for_verification=phone_number_for_verification+value;
        mPhoneNumberField.setText(phone_number_for_verification);
    }

    private void serviceIdTextValue(String value) {
        serviceIdFromTextField=serviceIdFromTextField+value;
        mServiceIdFieldText.setText(serviceIdFromTextField);
    }

    private void serviceIdDeleteValue() {
        String phonString=mServiceIdFieldText.getText().toString();
        if(phonString.length()>0) {
            serviceIdFromTextField = phonString.substring(0, phonString.length() - 1);
            mServiceIdFieldText.setText(serviceIdFromTextField);
        }else {
            mServiceIdFieldText.setText("");
            serviceIdFromTextField="";
        }
    }

    private void serviceIdOkValue(){
        appServiceId=mServiceIdFieldText.getText().toString();
        crypto = new Crypto(token);
        startHttpApiRequest();
        mDetailText.setText("Successfully verified your phone, Click continue");
        mDetailText.setTextColor(getResources().getColor(R.color.status_text_color_green));
        viewVisibilityVisible(continueBtn);

        //Toast.makeText(MainActivity.this,"clicked: "+mServiceIdFieldText.getText().toString(),Toast.LENGTH_SHORT).show();
    }

    //checking user permission
    protected boolean checkPermission(){
        if(ContextCompat.checkSelfPermission(this,Manifest.permission.CAMERA)
                + ContextCompat.checkSelfPermission(
                this,Manifest.permission.RECORD_AUDIO)
                + ContextCompat.checkSelfPermission(
                this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){

            // Do something, when permissions not granted
            if(ActivityCompat.shouldShowRequestPermissionRationale(
                    this,Manifest.permission.CAMERA)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    this,Manifest.permission.RECORD_AUDIO)
                    || ActivityCompat.shouldShowRequestPermissionRationale(
                    this,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                // If we should give explanation of requested permissions


                ActivityCompat.requestPermissions(
                        MainActivity.this,
                        new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.RECORD_AUDIO,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        },
                        MY_PERMISSIONS_REQUEST_CODE
                );

            }else{
                // Directly request for required permissions, without explanation
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.RECORD_AUDIO,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        },
                        MY_PERMISSIONS_REQUEST_CODE
                );
            }
        }else {
            return true;
            // Do something, when permissions are already granted
            //Toast.makeText(this,"Permissions already granted",Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    //user permission result
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CODE:{
                // When request is cancelled, the results array are empty
                if(
                        (grantResults.length >0) &&
                                (grantResults[0]
                                        + grantResults[1]
                                        + grantResults[2]
                                        == PackageManager.PERMISSION_GRANTED
                                )
                        ){
                    // Permissions are granted
                    //Toast.makeText(this,"Permissions granted.",Toast.LENGTH_SHORT).show();
                }else {
                    // Permissions are denied
                    //Toast.makeText(this,"Permissions denied.",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    //checking internet is connected or not
    public boolean isConnected() {
        boolean connected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
            return connected;
        } catch (Exception e) {
            Log.e("Connectivity Exception", e.getMessage());
        }
        return connected;
    }

    //custom keypad checked the verification text Field is null or full.
    // is null then set value here from first to last
    public void clickedValue(String value){

        if (code_text_1.getText().toString().length()==0){
            code_text_1.setText(value);
            code_text_2.setBackground(getDrawable(R.drawable.textview_focusable));
            code_text_1.setBackground(getDrawable(R.drawable.number_back));
            s1= code_text_1.getText().toString();
            text_ok.setImageResource(R.drawable.icon_ok_gray);
            viewClickableFalse(text_ok);

        }else if (code_text_2.getText().toString().length()==0){
            text_ok.setImageResource(R.drawable.icon_ok_gray);
            code_text_3.setBackground(getDrawable(R.drawable.textview_focusable));
            code_text_2.setBackground(getDrawable(R.drawable.number_back));
            viewClickableFalse(text_ok);
            code_text_2.setText(value);
            s2= code_text_2.getText().toString();

        }else if (code_text_3.getText().toString().length()==0){
            text_ok.setImageResource(R.drawable.icon_ok_gray);
            code_text_4.setBackground(getDrawable(R.drawable.textview_focusable));
            code_text_3.setBackground(getDrawable(R.drawable.number_back));
            viewClickableFalse(text_ok);
            code_text_3.setText(value);
            s3= code_text_3.getText().toString();

        }else if (code_text_4.getText().toString().length()==0){
            code_text_5.setBackground(getDrawable(R.drawable.textview_focusable));
            code_text_4.setBackground(getDrawable(R.drawable.number_back));
            text_ok.setImageResource(R.drawable.icon_ok_gray);
            viewClickableFalse(text_ok);
            code_text_4.setText(value);
            s4= code_text_4.getText().toString();

        }else if (code_text_5.getText().toString().length()==0){
            code_text_5.setBackground(getDrawable(R.drawable.number_back));
            code_text_6.setBackground(getDrawable(R.drawable.textview_focusable));
            text_ok.setImageResource(R.drawable.icon_ok_gray);
            viewClickableFalse(text_ok);
            code_text_5.setText(value);
            s5= code_text_5.getText().toString();

        }else if (code_text_6.getText().toString().length()==0){
            code_text_6.setBackground(getDrawable(R.drawable.number_back));
            viewClickableTrue(text_ok);
            text_ok.setImageResource(R.drawable.icon_ok_app_color);
            code_text_6.setText(value);
            s6= code_text_6.getText().toString();

        }
    }

    //custom keypad checked the verification text Field is null or full.
    // is not null then delete value here from last to first
    public void deleteValue(){
        viewClickableFalse(text_ok);
        text_ok.setImageResource(R.drawable.icon_ok_gray);

        if (code_text_6.getText().toString().length()>0){
            code_text_6.setBackground(getDrawable(R.drawable.textview_focusable));
            code_text_6.setText("");
            s6="";
        }else if (code_text_5.getText().toString().length()>0){
            code_text_6.setBackground(getDrawable(R.drawable.number_back));
            code_text_5.setBackground(getDrawable(R.drawable.textview_focusable));
            code_text_5.setText("");
            s5="";
        }else if (code_text_4.getText().toString().length()>0){
            code_text_5.setBackground(getDrawable(R.drawable.number_back));
            code_text_4.setBackground(getDrawable(R.drawable.textview_focusable));
            code_text_4.setText("");
            s4="";
        }else if (code_text_3.getText().toString().length()>0){
            code_text_4.setBackground(getDrawable(R.drawable.number_back));
            code_text_3.setBackground(getDrawable(R.drawable.textview_focusable));
            code_text_3.setText("");
            s3="";
        }else if (code_text_2.getText().toString().length()>0){
            code_text_3.setBackground(getDrawable(R.drawable.number_back));
            code_text_2.setBackground(getDrawable(R.drawable.textview_focusable));
            code_text_2.setText("");
            s2="";
        }else if (code_text_1.getText().toString().length()>0){
            code_text_2.setBackground(getDrawable(R.drawable.number_back));
            code_text_1.setBackground(getDrawable(R.drawable.textview_focusable));
            code_text_1.setText("");
            s1="";
        }
    }

    //return 6 digit's string of verification code
    private String verificationCodeString() {
        String s=s1+s2+s3+s4+s5+s6;
        return s;
    }

    ////http request
    private void startHttpApiRequest(){
        JSONObject msg = new JSONObject();
        try {
            msg.put("phone", phone);
            msg.put("shortcode", appServiceId);
            String encMsg = bytesToHex(crypto.encrypt(msg.toString()));

            String url = "http://seeds.synopi.com:8002/resolve/" + encMsg + "/" + token;

            RequestQueue queue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, mResponseListener, mErrorListener);
            queue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //getting api url
    private void onAppApiUrl(String app_url){
        Log.d("TAG", "onRtmpUrl: app_url: "+app_url);
        JSONObject msg = new JSONObject();
        String encMsg = null;
        try {
            msg.put("phone", phone);
            encMsg = bytesToHex(crypto.encrypt(msg.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String url = app_url + "/" + encMsg + "/" + token;
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, mResponseListener, mErrorListener);
        queue.add(stringRequest);
    }

    //getting rtmp url
    private void onRtmpUrl(String rtmp_url){
        Log.d("TAG", "onRtmpUrl: "+rtmp_url);
        rtmpUrl_main=rtmp_url;
    }

    //bytes To Hex converting
    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    //hexString To ByteArray converting
    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len/2];

        for(int i = 0; i < len; i+=2){
            data[i/2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
        }

        return data;
    }

    //response for http request
    private Response.Listener mResponseListener = new Response.Listener() {
        @Override
        public void onResponse(Object response) {
            byte[] data = hexStringToByteArray(response.toString());
            Log.d("TAG", "onRtmpUrl: data: "+data.toString());
            try {
                byte[] dec = crypto.decrypt(data, 0, data.length);
                String str = new String(dec);
                JSONObject msg = new JSONObject(str);
                if(msg.has("api_url")) {
                    String appApiUrl = msg.getString("api_url");
                    Log.d("TAG", "onRtmpUrl: appApiUrl: "+appApiUrl);
                    onAppApiUrl(appApiUrl);
                }else if(msg.has("rtmp_url")){
                    rtmpUrl_main = msg.getString("rtmp_url");
                    Log.d("TAG", "onRtmpUrl: rtmpUrl: "+rtmpUrl_main);
                    //onRtmpUrl(rtmpUrl);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private Response.ErrorListener mErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d("TAG", "Http request error: "+error.getMessage());
        }
    };


    @Override
    public void onCountrySelected() {
        countryPhoneCode=cppSpinnerPhoneCode.getSelectedCountryCodeWithPlus();
        phoneCodeText.setText(countryPhoneCode);
        Log.d("TAG", "onCountrySelected: "+countryPhoneCode);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                Log.d(TAG, "onTextChanged: changed");
                if(mServiceIdFieldText.getText().toString().length()==0){
                    Log.d(TAG, "onTextChanged: changed==0 before");
                    mServiceIdOkBtn.setTextColor(getResources().getColor(R.color.button_ok_color));
                    viewClickableFalse(mServiceIdOkBtn,text_ok);
                    text_ok.setImageResource(R.drawable.icon_ok_gray);
                    //viewVisibilityGone(clearImgBtn,clearLayBtn);
                    mServiceIdOkBtn.setBackground(getDrawable(R.drawable.custom_button));
                }
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (mPhoneNumberField.getVisibility()==View.VISIBLE) {

                if (mPhoneNumberField.getText().toString().length() > 0) {
                    viewClickableTrue(mStartButton,text_ok);
                    text_ok.setImageResource(R.drawable.icon_ok_app_color);
                    mStartButton.setBackground(getDrawable(R.drawable.custom_button_aoo_color));
                    mStartButton.setTextColor(getResources().getColor(R.color.white));
                    //viewVisibilityVisible(clearImgBtn, clearLayBtn);
                } else {
                    mStartButton.setTextColor(getResources().getColor(R.color.appColor));
                    viewClickableFalse(mStartButton,text_ok);
                    text_ok.setImageResource(R.drawable.icon_ok_gray);
                    //viewVisibilityGone(clearImgBtn, clearLayBtn);
                    mStartButton.setBackground(getDrawable(R.drawable.custom_button));
                }
            } else if(/*numberCode*/ mVerificationField.getVisibility()==View.VISIBLE){
                if(mVerificationField.getText().toString().length()>=6){
//                if(code_text_6.getText().toString().length()>0){
                    text_ok.setImageResource(R.drawable.icon_ok_app_color);
                    viewClickableTrue(mVerifyButton,text_ok);
                    mVerifyButton.setBackground(getDrawable(R.drawable.custom_button_aoo_color));
                    mVerifyButton.setTextColor(getResources().getColor(R.color.white));
                    //viewVisibilityVisible(clearImgBtn,clearLayBtn);
                }else{
                    mVerifyButton.setTextColor(getResources().getColor(R.color.button_ok_color));
                    viewClickableFalse(mVerifyButton,text_ok);
                    text_ok.setImageResource(R.drawable.icon_ok_gray);
                    //viewVisibilityGone(clearImgBtn,clearLayBtn);
                    mVerifyButton.setBackground(getDrawable(R.drawable.custom_button));
                }
            }
            if(mServiceIdFieldText.getVisibility()==View.VISIBLE){
                Log.d(TAG, "onTextChanged: changed");
                if(mServiceIdFieldText.getText().toString().length()>0){
                    Log.d(TAG, "onTextChanged: changed>0");
                    text_ok.setImageResource(R.drawable.icon_ok_app_color);
                    viewClickableTrue(mServiceIdOkBtn,text_ok);
                    mServiceIdOkBtn.setBackground(getDrawable(R.drawable.custom_button_aoo_color));
                    mServiceIdOkBtn.setTextColor(getResources().getColor(R.color.white));
                    //viewVisibilityVisible(clearImgBtn,clearLayBtn);
                }else{
                    Log.d(TAG, "onTextChanged: changed==0");
                    mServiceIdOkBtn.setTextColor(getResources().getColor(R.color.button_ok_color));
                    viewClickableFalse(mServiceIdOkBtn,text_ok);
                    text_ok.setImageResource(R.drawable.icon_ok_gray);
                    //viewVisibilityGone(clearImgBtn,clearLayBtn);
                    mServiceIdOkBtn.setBackground(getDrawable(R.drawable.custom_button));
                }
            }

//            if(numberCode.getVisibility()==View.VISIBLE){
//                if()
//            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

//    @Override
//    public boolean onTouch(View view, MotionEvent motionEvent) {
//        switch (view.getId()){
//            case R.id.field_phone_number:
//                //imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                break;
//            case R.id.serviceId_textView_Id:
//                //imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                break;
//        }
//        return false;
//    }

    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();
    private long timeCount = 0;
public int doSomeTasks() {

    while (timeCount <= 1000000) {

        timeCount++;

        if (timeCount == 100000) {
            return 10;
        } else if (timeCount == 200000) {
            return 20;
        } else if (timeCount == 300000) {
            return 30;
        }
    }

    return 100;

}
}
