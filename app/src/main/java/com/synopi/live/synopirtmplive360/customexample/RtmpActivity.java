package com.synopi.live.synopirtmplive360.customexample;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pedro.encoder.input.video.CameraHelper;
import com.pedro.encoder.input.video.CameraOpenException;
import com.pedro.rtplibrary.rtmp.RtmpCamera1;
import com.synopi.live.synopirtmplive360.NetworkChangeReceiver;
import com.synopi.live.synopirtmplive360.R;
import com.synopi.live.synopirtmplive360.VisibleToggleClickListener;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.extra.Scale;

import net.ossrs.rtmp.ConnectCheckerRtmp;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import me.anwarshahriar.calligrapher.Calligrapher;


public class RtmpActivity extends AppCompatActivity implements Button.OnClickListener, ConnectCheckerRtmp, SurfaceHolder.Callback, View.OnTouchListener {

    private Integer[] orientations = new Integer[] { 0, 90, 180, 270 };
    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;

    int orientation;

    private RtmpCamera1 rtmpCamera1;
    private ImageView streamOnOffIcon;


    TextView label;
    private String currentDateAndTime = "";
    private File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/RTMP_RECORDINGS");

    private RelativeLayout urlRelativeLayout;

    private int currentApiVersion;
    ImageView switchCamera,redIconImage;
    ImageView cameraControl,microphoneControl,settingControl;

    // stream video
    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    private Handler handler = new Handler();
    int Seconds, Minutes, MilliSeconds,Hours ;


    boolean streaming =false;

    //settings views finding
    RelativeLayout settingUrlLayout, settingAudioBitLayout,settingAudioSampleRateLayout,settingAudioChannelLayout;
    RelativeLayout settingVideoFrameRateLayout,settingVideoBitLayout,settingVideoResolutionLayout,settingVideoCodecLayout;
    RelativeLayout settingRecordingLayout,settingCameraModeLayout,settingCameraFaceLayout;

    //edittext from alert dialog
    EditText alert_audio_bitrate,alert_video_bitrate,alert_video_framerate;

    //textviews of settings
    TextView url_setting_text,bitrate_video_setting_text,resolution_video_setting_text,framerate_video_setting_text,codec_setting_text;
    TextView samplerate_audio_setting_text,bitrate_audio_setting_text,channel_audio_setting_text,recording_setting_text;
    TextView camera_mode_setting_text,camera_facing_setting_text;

    //Resolution
    String[] resolution= new String[110];
    int totalDisplayResolution=0;
    RadioGroup resolutionRadioGroup;

    //audio sample rate radio alert
    RadioButton sample_radio_1, sample_radio_2, sample_radio_3, sample_radio_4, sample_radio_5;
    String samplerateSelected;

    //audio channel radio alert
    RadioButton channel_radio_1, channel_radio_2;
    String channelSelected;

    //recording radio alert
    RadioButton record_radio_1, record_radio_2;
    String recordSelected;
    boolean willRecord=false;

    //video codec radio alert
    RadioButton radio_h264, radio_h265;
    String codecSelected;

    //camera angle radio alert
    RadioButton radio_mode_portrait, radio_mode_landscape,radio_mode_landscape_reverse;
    String cameraModeSelected;

    //value used when streaming
    int a_bit_rate=64, v_bit_rate=700,v_res_height=720,v_res_width=480,v_frame_rate=30;
    int a_sample_rate=44100;
    boolean isStereo=true,echoChanceler=false,noiseSuppressor=false,is264codec=true;

    boolean doubleBackToExitPressedOnce = false;
    //surface view
    SurfaceView surfaceView;

    //for snackbar
    private BroadcastReceiver mNetworkReceiver;
    static RelativeLayout relativeLayout;
    static Snackbar snackbar;
    static View snackView;
    static TextView textView;
    static String connectionFromFirstActivity;
    static boolean isOffline=false;


    //storing stream url & key into shared preferences
    //declaring sharedpreferences
    SharedPreferences sharedpreferences;
    public static final String myPreference = "myprefUrl";
    public static final String Stream_Url = "streamUrlShare";
    public static final String Stream_v_codec = "streamVideoCodec";
    public static final String Stream_v_resolution = "streamVideoRes";
    public static final String Stream_v_bitrate = "streamVideoBit";
    public static final String Stream_v_framerate = "streamVideoFrame";
    public static final String Stream_a_samplerate = "streamAudioSample";
    public static final String Stream_a_bitrate = "streamAudioBit";
    public static final String Stream_a_channel = "streamAudioChan";
    public static final String Stream_record = "streamRecord";
    public static final String Stream_camera_mode = "cameraAngle";
    public static final String Stream_camera_type = "cameraType";
    public static final String varified_phone = "phoneVarify";
    public static final String varifier_name = "nameVarify";
    public static final String varifier_serviceId = "serviceIdVarify";

    //setting layout width fixed with scrollable depending on screen orientation
    RelativeLayout rl_custom;
    Calligrapher calligrapher;
    ViewGroup.LayoutParams params_1;
   // ViewGroup.LayoutParams params_2;
    float scale_1;
    LinearLayout setting_linear_lay;
    int orient_screen;

    String PHONE_NO="",RTMP_URL="",SERVICE_ID="",USER_NAME="",continueBtnId="",CAMERA_ANGLE="";


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_custom);

        // adding font family for all views
        calligrapher=new Calligrapher(this);
        calligrapher.setFont(this,"raleway_regular.ttf",true);

        sharedpreferences = this.getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        //1 for connected & 0 for disconnected
        connectionFromFirstActivity= getIntent().getStringExtra("connected");
        Log.d("TAG", "onCreate: connectionFromFirstActivity: "+connectionFromFirstActivity);

        continueBtnId= getIntent().getStringExtra("continueBtnId");
        if(continueBtnId.equals("")){
            PHONE_NO=getVarifiedPhone();
            RTMP_URL=getUrlStream();
            SERVICE_ID=getVarifierServiceId();
            USER_NAME=getVarifierName();

        }else{
            PHONE_NO= getIntent().getStringExtra("phoneNumber");
            RTMP_URL= getIntent().getStringExtra("rtmpLink");
            SERVICE_ID= getIntent().getStringExtra("serviceId");
            USER_NAME= getIntent().getStringExtra("username");

            CAMERA_ANGLE=getIntent().getStringExtra("Portrait");
            saveUrlStream(RTMP_URL);
            saveVarifiedPhone(PHONE_NO);
            saveVarifierName(USER_NAME);
            saveVarifierServiceId(SERVICE_ID);
            saveSelectedCameraMode("Portrait");
        }

        Log.d("TAG", "onCreate: phone: "+PHONE_NO);
        Log.d("TAG", "onCreate: phone: "+RTMP_URL);
        Log.d("TAG", "onCreate: phone: "+SERVICE_ID);
        Log.d("TAG", "onCreate: phone: "+USER_NAME);



        try {
            if (!folder.exists()) {
                folder.mkdir();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        //for full screen view
        currentApiVersion = Build.VERSION.SDK_INT;
        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        if (currentApiVersion >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView().setSystemUiVisibility(flags);
            final View decorView = getWindow().getDecorView();
            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                        decorView.setSystemUiVisibility(flags);
                    }
                }
            });
        }

        //finding surface view
        surfaceView = findViewById(R.id.surfaceView);
        surfaceView.getHolder().addCallback(this);

        surfaceView.setOnTouchListener(this);
        rtmpCamera1 = new RtmpCamera1(surfaceView, this);

        relativeLayout = findViewById(R.id.activity_custom);

        //snackbar for connectivity of internet
        snackbar=Snackbar.make(relativeLayout,"",Snackbar.LENGTH_INDEFINITE);
        snackView=snackbar.getView();
        textView = snackView.findViewById(android.support.design.R.id.snackbar_text);

        //checking connection through broadcast receiver
        mNetworkReceiver = new NetworkChangeReceiver();
        registerNetworkBroadcastForNougat();

        //controlling icon
        cameraControl=findViewById(R.id.camera);
        microphoneControl=findViewById(R.id.microphone);
        settingControl=findViewById(R.id.settingIconId);
        urlRelativeLayout=findViewById(R.id.urlLayoutId);
        label=findViewById(R.id.live_label);
        redIconImage=findViewById(R.id.redIcon);
        streamOnOffIcon = findViewById(R.id.b_start_stop);
        switchCamera = findViewById(R.id.switch_camera);

        //finding setting layout for url
        settingUrlLayout=findViewById(R.id.setting_url_id);

        //finding setting layout for audio
        settingAudioBitLayout=findViewById(R.id.setting_audio_bit_id);
        settingAudioSampleRateLayout=findViewById(R.id.setting_audio_sample_id);
        settingAudioChannelLayout=findViewById(R.id.setting_audio_channel_id);

        //finding setting layout for recording
        settingRecordingLayout=findViewById(R.id.setting_video_record_id);

        //finding setting layout for video
        settingVideoFrameRateLayout=findViewById(R.id.setting_video_frame_id);
        settingVideoBitLayout=findViewById(R.id.setting_video_bit_id);
        settingVideoResolutionLayout=findViewById(R.id.setting_video_res_id);
        settingVideoCodecLayout=findViewById(R.id.setting_video_codec_id);

        //finding layout for camera setting
        settingCameraFaceLayout=findViewById(R.id.setting_cam_face_id);
        settingCameraModeLayout=findViewById(R.id.setting_cam_angle_id);
        camera_facing_setting_text=findViewById(R.id.camera_fetch_setting_text_id);
        camera_mode_setting_text=findViewById(R.id.camera_angle_setting_text_id);
        url_setting_text=findViewById(R.id.url_setting_text_id);
        bitrate_video_setting_text=findViewById(R.id.bitrate_video_setting_text_id);
        resolution_video_setting_text=findViewById(R.id.resolution_setting_text_id);
        framerate_video_setting_text=findViewById(R.id.framerate_video_setting_text_id);
        codec_setting_text=findViewById(R.id.codec_setting_text_id);
        samplerate_audio_setting_text=findViewById(R.id.sample_setting_text_id);
        bitrate_audio_setting_text=findViewById(R.id.bitrate_audio_setting_text_id);
        channel_audio_setting_text=findViewById(R.id.channel_audio_setting_text_id);
        recording_setting_text=findViewById(R.id.record_video_setting_text_id);
        setting_linear_lay=findViewById(R.id.setting_linear_layout);

        //setting layout width fixed with scrollable depending on screen orientation
        //height of setting layout
        params_1=  setting_linear_lay.getLayoutParams();
        //height of resolution alert dialog
        //params_2=  setting_linear_lay.getLayoutParams();
        scale_1 = this.getResources().getDisplayMetrics().density;
        orient_screen=getWindowManager().getDefaultDisplay().getRotation();
        if(orient_screen==0) {
            params_1.height = ((int) (scale_1 * 33 + 0.5f)) * 12;
            //params_2.height = ((int) (scale_1 * 33 + 0.5f)) * 8;

        }else{
            params_1.height = ((int) (scale_1 * 33 + 0.5f)) * 7;
            //params_2.height = ((int) (scale_1 * 33 + 0.5f)) * 4;
        }



        Log.d("TAG", "onCreate: o: "+orient_screen+" hei: "+params_1.height);
        cameraControl.setOnClickListener(this);
        microphoneControl.setOnClickListener(this);
        settingControl.setOnClickListener(this);
        streamOnOffIcon.setOnClickListener(this);
        switchCamera.setOnClickListener(this);
        camera_facing_setting_text.setText("Rear Camera");

        viewVisibilityInvisible(redIconImage);
        viewClickableFalse(settingUrlLayout);


        for (Camera.Size size : rtmpCamera1.getResolutionsBack()) {
            resolution[totalDisplayResolution]=size.width + "X" + size.height;
            totalDisplayResolution++;
        }

        setOrientation();

        defaultSettingFromSharedPreferences();



        //single line & last at 3 dots for url text
        url_setting_text.setSingleLine(true);
        url_setting_text.setEllipsize(TextUtils.TruncateAt.END);
        String uurrll=getUrlStream();
        url_setting_text.setText(uurrll);


        //clicking for alert dialog
        settingVideoResolutionLayout.setOnClickListener(this);
        settingVideoCodecLayout.setOnClickListener(this);
        settingVideoBitLayout.setOnClickListener(this);
        settingVideoFrameRateLayout.setOnClickListener(this);
        settingRecordingLayout.setOnClickListener(this);
        settingAudioChannelLayout.setOnClickListener(this) ;
        settingAudioSampleRateLayout.setOnClickListener(this) ;
        settingAudioBitLayout.setOnClickListener(this);
        settingCameraModeLayout.setOnClickListener(this);


        @SuppressLint("CutPasteId") final ViewGroup transitionsContainer = (ViewGroup) findViewById(R.id.activity_custom);
        rl_custom = (RelativeLayout) transitionsContainer.findViewById(R.id.urlLayoutId);

        findViewById(R.id.settingIconId).setOnClickListener(new VisibleToggleClickListener() {

            @Override
            protected void changeVisibility(boolean visible) {
                //getSettingsLayVisible();
                if(rtmpCamera1.isStreaming()){
                    //snackbar for connectivity of internet
                    Snackbar snackbarSet=Snackbar.make(relativeLayout,"",Snackbar.LENGTH_INDEFINITE);
                    View snackViewSet=snackbarSet.getView();
                    TextView textSnack = snackViewSet.findViewById(android.support.design.R.id.snackbar_text);

                    snackbarSet.setDuration(Snackbar.LENGTH_LONG);
                    snackViewSet.setBackgroundColor(Color.parseColor("#e9c420"));
                    textSnack.setText("Can't access settings menu when streaming");
                    textSnack.setTextColor(Color.WHITE);
                    textSnack.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    snackbarSet.show();

                }else {
                    TransitionManager.beginDelayedTransition(transitionsContainer, new Scale());
                    if(getSettingsLayVisible()){
                        viewVisibilityVisible(rl_custom);
                        viewVisibilityGone(streamOnOffIcon);
                        viewClickableFalse(microphoneControl,cameraControl,switchCamera,streamOnOffIcon);
                    }else{
                        setOrientation();
                        viewVisibilityGone(rl_custom);
                        viewVisibilityVisible(streamOnOffIcon);
                        viewClickableTrue(microphoneControl,cameraControl,switchCamera,streamOnOffIcon);

                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);
                    }
                }

            }

        });

        findViewById(R.id.settingCancelCrossButton).setOnClickListener(new VisibleToggleClickListener() {

            @Override
            protected void changeVisibility(boolean visible) {
                if(rtmpCamera1.isStreaming()){
                    //snackbar for connectivity of internet
                    Snackbar snackbarSet=Snackbar.make(relativeLayout,"",Snackbar.LENGTH_INDEFINITE);
                    View snackViewSet=snackbarSet.getView();
                    TextView textSnack = snackViewSet.findViewById(android.support.design.R.id.snackbar_text);

                    snackbarSet.setDuration(Snackbar.LENGTH_LONG);
                    snackViewSet.setBackgroundColor(Color.parseColor("#e9c420"));
                    textSnack.setText("Can't access settings menu when streaming");
                    textSnack.setTextColor(Color.WHITE);
                    textSnack.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    snackbarSet.show();

                }else {
                    TransitionManager.beginDelayedTransition(transitionsContainer, new Scale());
                    if(getSettingsLayVisible()){
                        viewVisibilityVisible(rl_custom);
                        viewVisibilityGone(streamOnOffIcon);
                        viewClickableFalse(microphoneControl,cameraControl,switchCamera,streamOnOffIcon);
                    }else{
                        setOrientation();
                        viewVisibilityGone(rl_custom);
                        viewVisibilityVisible(streamOnOffIcon);
                        viewClickableTrue(microphoneControl,cameraControl,switchCamera,streamOnOffIcon);

                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);
                    }
                }

            }

        });

    }

    //setting data from shared preferences or default
    private void defaultSettingFromSharedPreferences() {

        //saved camera mode/angle
        String modeCam=getSelectedCameraMode();
        Log.d("TAG", "\nsharedpreferences: modeCam:"+modeCam);
        if(modeCam.equals("")){
            camera_mode_setting_text.setText("Portrait");
            saveSelectedCameraMode("Portrait");
        }else{
            camera_mode_setting_text.setText(modeCam);
        }

        //saved video bitrate
        String vvBit=getVideoBitrate();
        if(vvBit.equals("")){
            saveVideoBitrate("1000 kbps");
            bitrate_video_setting_text.setText(R.string.video_bitrate_text);
            v_bit_rate=1000;
        }else{
            bitrate_video_setting_text.setText(vvBit);
            v_bit_rate=Integer.parseInt(vvBit.replace(" kbps",""));
        }

        //video frame
        String vvframe=getVideoFramerate();
        if(vvframe.equals("")){
            saveVideoFramerate("25 fps");
            framerate_video_setting_text.setText(R.string.video_framerate_text);
            v_frame_rate=25;
        }else{
            framerate_video_setting_text.setText(vvframe);
            v_frame_rate=Integer.parseInt(vvframe.replace(" fps",""));
        }

        //video codec
        String vvCodec=getVideoCodec();
        if(vvCodec.equals("")){
            codec_setting_text.setText(R.string.video_codec_text_264);
            vvCodec="H 264";
        }else{
            codec_setting_text.setText(vvCodec);
            vvCodec="H 264";
        }

        //audio sample rate
        String aaSample=getAudioSamplerate();
        if(aaSample.equals("")){
            samplerate_audio_setting_text.setText(R.string.audio_samplerate_text);
        }else{
            samplerate_audio_setting_text.setText(aaSample);
        }

        //audio bit rate
        String aaBit=getAudioBitrate();
        if(aaBit.equals("")){
            saveAudioBitrate("64 kbps");
            bitrate_audio_setting_text.setText(R.string.audio_bitrate_text);
            a_bit_rate=64;
        }else{
            bitrate_audio_setting_text.setText(aaBit);
            a_bit_rate=Integer.parseInt(aaBit.replace(" kbps",""));
        }

        //audio channel
        String aaChan=getAudioChannel();
        if(aaChan.equals("")){
            channel_audio_setting_text.setText(R.string.stereo);
            isStereo=true;
            saveAudioChannel("Stereo");
        }else{
            channel_audio_setting_text.setText(aaChan);
            if(aaChan.equals("Mono")) {
                isStereo=false;
            }else{
                isStereo=true;
            }
        }

        //video recording or not
        String reco=getRecording();
        if(reco.equals("")){
            recording_setting_text.setText("Disabled");
            saveRecording("Disabled");
        }else{
            recording_setting_text.setText(reco);
        }

        viewVisibilityGone(urlRelativeLayout);
        viewVisibilityVisible(streamOnOffIcon);

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);


//        if(mySqLiteDatabaseHelper.rowCountFromTable()<1){
//            mySqLiteDatabaseHelper.insertData("","","");
//        }


        //video resolution
        String vvRes=getVideoResolution();
        if(vvRes.equals("")){
            String ss=resolution[4];
            resolution_video_setting_text.setText(ss);
            saveVideoResolution(ss);

            int possi=0;
            for(int i=ss.length()-1;i>=0;i--){
                char a=ss.charAt(i);
                String a_s=String.valueOf(a);
                if(a_s.equals("X")){
                    possi=i;
                    break;
                }
            }
            v_res_height=Integer.parseInt(ss.substring(0,possi));
            v_res_width=Integer.parseInt(ss.substring(possi+1,ss.length()));

        }else{
            resolution_video_setting_text.setText(vvRes);
            int possi=0;
            for(int i=vvRes.length()-1;i>=0;i--){
                char a=vvRes.charAt(i);
                String a_s=String.valueOf(a);
                if(a_s.equals("X")){
                    possi=i;
                    break;
                }
            }
            v_res_height=Integer.parseInt(vvRes.substring(0,possi));
            v_res_width=Integer.parseInt(vvRes.substring(possi+1,vvRes.length()));
        }
    }

    //clicking event for different view
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.microphone:
                if (!rtmpCamera1.isAudioMuted()) {
                    microphoneControl.setImageResource(R.drawable.muted_mic);
                    Toast.makeText(RtmpActivity.this,"Mic is Muted",Toast.LENGTH_SHORT).show();
                    rtmpCamera1.disableAudio();
                } else {
                    microphoneControl.setImageResource(R.drawable.microphone_mic);
                    Toast.makeText(RtmpActivity.this,"Mic is Unmuted",Toast.LENGTH_SHORT).show();
                    rtmpCamera1.enableAudio();
                }
                break;
            case R.id.camera:
                if (rtmpCamera1.isVideoEnabled()) {
                    cameraControl.setImageResource(R.drawable.cam_off);
                    Toast.makeText(RtmpActivity.this,"Camera is Off",Toast.LENGTH_SHORT).show();
                    rtmpCamera1.disableVideo();
                } else {
                    Toast.makeText(RtmpActivity.this,"Camera is On",Toast.LENGTH_SHORT).show();
                    cameraControl.setImageResource(R.drawable.cam_on);
                    rtmpCamera1.enableVideo();
                }
                break;

            case R.id.b_start_stop:
                if(isConnected()){
                    if (!rtmpCamera1.isStreaming()) {
                        willRecord=false;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
                        currentDateAndTime = sdf.format(new Date());
                        if (prepareEncoders()) {
                            rtmpCamera1.startStream(url_setting_text.getText().toString());
                        }
                    } else {;
                        rtmpCamera1.stopRecord();
                        currentDateAndTime = "";
                        stopCountingLive();
                        rtmpCamera1.stopStream();
                        streamOnOffIcon.setImageResource(R.drawable.synopi_live_video_white);
                        streaming=false;
                    }
                }else{
                    rtmpCamera1.stopRecord();
                    currentDateAndTime = "";
                    stopCountingLive();
                    rtmpCamera1.stopStream();
                    streamOnOffIcon.setImageResource(R.drawable.synopi_live_video_white);
                    streaming=false;
                }
                break;

            case R.id.switch_camera:
                try {

                    if(rtmpCamera1.isFrontCamera()){
                        camera_facing_setting_text.setText("Rear Camera");
                        rtmpCamera1.switchCamera();
                    }else{
                        camera_facing_setting_text.setText("Front Camera");
                        rtmpCamera1.switchCamera();
                    }
                } catch (final CameraOpenException e) {
                    //Toast.makeText(RtmpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.setting_cam_angle_id:
                cameraModeAlert(R.style.DialogAnimation);
                break;

            case R.id.setting_audio_bit_id:
                audioBitAlert(R.style.DialogAnimation);
                break;

            case R.id.setting_audio_sample_id:
                audioSampleAlert(R.style.DialogAnimation);
                break;

            case R.id.setting_audio_channel_id:
                audioChannelAlert(R.style.DialogAnimation);
                break;

            case R.id.setting_video_record_id:
                recordingAlert(R.style.DialogAnimation);
                break;

            case R.id.setting_video_frame_id:
                videoFrameAlert(R.style.DialogAnimation);
                break;

            case R.id.setting_video_bit_id:
                videoBitAlert(R.style.DialogAnimation);
                break;

            case R.id.setting_video_res_id:
                videoResolutionAlert(R.style.DialogAnimation);
                break;

            case R.id.setting_video_codec_id:
                videoCodecAlert(R.style.DialogAnimation);
                break;

            default:
                break;
        }
    }

    //Save service id
    public void saveVarifierServiceId(String v_s_id) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(varifier_serviceId, v_s_id);
        editor.commit();
    }

    //Retrieving service id
    public String getVarifierServiceId() {
        String v_s_id="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(varifier_serviceId)) {
            v_s_id=sharedpreferences.getString(varifier_serviceId, "");
        }
        return v_s_id;
    }

    //Save user name
    public void saveVarifierName(String v_name) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(varifier_name, v_name);
        editor.commit();
    }

    //Retrieving user name
    public String getVarifierName() {
        String v_name="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(varifier_name)) {
            v_name=sharedpreferences.getString(varifier_name, "");
        }
        return v_name;
    }

    //Save phone number
    public void saveVarifiedPhone(String v_phone) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(varified_phone, v_phone);
        editor.commit();
    }

    //Retrieving phone number
    public String getVarifiedPhone() {
        String v_phone="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(varified_phone)) {
            v_phone=sharedpreferences.getString(varified_phone, "");
        }
        return v_phone;
    }

    public boolean getSettingsLayVisible(){
        boolean visi=true;
        if(rl_custom.isShown()){
            visi=false;
        }
        Log.d("TAG", "getSettingsLayVisiblejjj: "+visi);
        return visi;
    }

    //Stream url & key saved
    public void saveUrlStream(String uurl) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Stream_Url, uurl);
        editor.commit();
    }

    //Retrieving stream url & key
    public String getUrlStream() {
        String uurl="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(Stream_Url)) {
            uurl=sharedpreferences.getString(Stream_Url, "");
        }
        return uurl;
    }

    //video Stream resolution saved
    public void saveVideoResolution(String resolu) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Stream_v_resolution, resolu);
        editor.commit();
    }

    //Retrieving video stream resolution
    public String getVideoResolution() {
        String resolu="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(Stream_v_resolution)) {
            resolu=sharedpreferences.getString(Stream_v_resolution, "");
        }
        return resolu;
    }

    //video Stream bitrate saved
    public void saveVideoBitrate(String videoBit) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Stream_v_bitrate, videoBit);
        editor.commit();
    }

    //Retrieving video stream bitrate
    public String getVideoBitrate() {
        String videoBit="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(Stream_v_bitrate)) {
            videoBit=sharedpreferences.getString(Stream_v_bitrate, "");
        }
        return videoBit;
    }

    //Stream video framerate saved
    public void saveVideoFramerate(String videoFrame) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Stream_v_framerate, videoFrame);
        editor.commit();
    }

    //Retrieving video stream framerate
    public String getVideoFramerate() {
        String videoFrame="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(Stream_v_framerate)) {
            videoFrame=sharedpreferences.getString(Stream_v_framerate, "");
        }
        return videoFrame;
    }

    //Stream video codec saved
    public void saveVideoCodec(String videoCodec) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Stream_v_codec, videoCodec);
        editor.commit();
    }

    //Retrieving video stream codec
    public String getVideoCodec() {
        String videoCodec="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(Stream_v_codec)) {
            videoCodec=sharedpreferences.getString(Stream_v_codec, "");
        }
        return videoCodec;
    }

    //Stream audio sampleratre saved
    public void saveAudioSamplerate(String audioSampleRate) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Stream_a_samplerate, audioSampleRate);
        editor.commit();
    }

    //Retrieving stream audio sampleratre
    public String getAudioSamplerate() {
        String audioSampleRate="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(Stream_a_samplerate)) {
            audioSampleRate=sharedpreferences.getString(Stream_a_samplerate, "");
        }
        return audioSampleRate;
    }

    //Stream audio bitrate saved
    public void saveAudioBitrate(String audioBit) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Stream_a_bitrate, audioBit);
        editor.commit();
    }

    //Retrieving stream audio bitrate
    public String getAudioBitrate() {
        String audioBit="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(Stream_a_bitrate)) {
            audioBit=sharedpreferences.getString(Stream_a_bitrate, "");
        }
        return audioBit;
    }

    //Stream audio channel saved
    public void saveAudioChannel(String audioChannel) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Stream_a_channel, audioChannel);
        editor.commit();
    }

    //Retrieving stream audio channel
    public String getAudioChannel() {
        String audioChannel="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(Stream_a_channel)) {
            audioChannel=sharedpreferences.getString(Stream_a_channel, "");
        }
        return audioChannel;
    }

    //camera mode/angle saved
    public void saveSelectedCameraMode(String mode) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Stream_camera_mode, mode);
        editor.commit();
    }

    //Retrieving camera mode/angle
    public String getSelectedCameraMode() {
        String cameraMode="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(Stream_camera_mode)) {
            cameraMode=sharedpreferences.getString(Stream_camera_mode, "");
        }
        return cameraMode;
    }

    //Stream audio channel saved
    public void saveRecording(String record) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Stream_record, record);
        editor.commit();
    }

    //Retrieving stream audio channel
    public String getRecording() {
        String record="";
        sharedpreferences = getSharedPreferences(myPreference, Context.MODE_PRIVATE);

        if (sharedpreferences.contains(Stream_record)) {
            record=sharedpreferences.getString(Stream_record, "");
        }
        return record;
    }

    //setting user manual screen orientation
    private void resetOrientation() {
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
    }

    //setting screen orientation
    private void setOrientation() {

        String cam_angle=getSelectedCameraMode();
        Log.d("TAG", "setOrientation_angle: "+cam_angle);
        if(cam_angle.equals("Portrait")){
            Log.d("TAG", "setOrientation_Portrait:"+cam_angle);

            try{
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }catch (Exception e){
                Log.d("TAG", "setOrientation: "+e.getMessage());
            }
        }else if(cam_angle.equals("Landscape")){
            Log.d("TAG", "setOrientation_Landscape: "+cam_angle);
            try{
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }catch (Exception e){
                Log.d("TAG", "setOrientation: "+e.getMessage());
            }

        }else if(cam_angle.equals("Reverse Landscape")){
            Log.d("TAG", "setOrientation_Reverse: "+cam_angle);
            try{
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
            }catch (Exception e){
                Log.d("TAG", "setOrientation: "+e.getMessage());
            }

        }
    }

    //video codec
    private void videoCodecAlert(int animationSource) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.codec_video_alert,null);
        //setting font
        //calligrapher.setFont(view,"raleway_regular.ttf");

        builder.setView(view);
        final android.app.AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;

        Button dialogButton = (Button) view.findViewById(R.id.alert_video_codec_ok_id);
        Button dialogButtonCancel = (Button) view.findViewById(R.id.alert_video_codec_cancel_id);

        radio_h264 = (RadioButton) view.findViewById(R.id.radio_id_h264);
        radio_h265 = (RadioButton) view.findViewById(R.id.radio_id_h265);

        if(codec_setting_text.getText().toString().equals("H 264")) {
            radio_h264.setChecked(true);
        }else{
            radio_h265.setChecked(true);
        }

        //if ok button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radio_h264.isChecked()) {
                    codecSelected = radio_h264.getText().toString();
                    is264codec=false;
                } else if (radio_h265.isChecked()) {
                    codecSelected = radio_h265.getText().toString();
                    is264codec=true;
                }

                codec_setting_text.setText(codecSelected);
                saveVideoCodec(codecSelected);
                dialog.dismiss();
            }
        });

        //if cancel button is clicked, close the custom dialog
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //camera screen orientation
        int ori=getWindowManager().getDefaultDisplay().getRotation();
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

        if(ori==0){
            wmlp.y = 350;
        }else{
            wmlp.y = 110;
        }
        dialog.show();
    }

    //video bitrate to and from alert dialog
    private void videoBitAlert(int animationSource) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.bitrate_video_alert,null);
        //setting font
        //calligrapher.setFont(view,"raleway_regular.ttf");
        builder.setView(view);
        final android.app.AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;

        final Button dialogButton = (Button) view.findViewById(R.id.alert_bit_video_ok_id);
        dialogButton.setTextColor(getResources().getColor(R.color.white));
        Button dialogButtoncancel = (Button) view.findViewById(R.id.alert_bit_video_cancel_id);
        alert_video_bitrate=view.findViewById(R.id.alert_bit_video_id);
        final ImageView cancelImageView= (ImageView) view.findViewById(R.id.btn_clear_keyId);
        final RelativeLayout cancelImageView_key= (RelativeLayout) view.findViewById(R.id.btn_clear_lay_keyId);

        alert_video_bitrate.setText(bitrate_video_setting_text.getText().toString().replace(" kbps","").trim());

        alert_video_bitrate.setHint("1000");

        cancelImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert_video_bitrate.setText("");
            }
        });

        cancelImageView_key.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert_video_bitrate.setText("");
            }
        });

        dialogButton.setBackground(getDrawable(R.drawable.custom_button_aoo_color));
        viewVisibilityVisible(cancelImageView);
        viewClickableTrue(dialogButton);

        alert_video_bitrate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(alert_video_bitrate.getText().toString().trim().length()>0) {
                    dialogButton.setTextColor(getResources().getColor(R.color.white));
                    dialogButton.setBackground(getDrawable(R.drawable.custom_button_aoo_color));
                    viewVisibilityVisible(cancelImageView);
                    viewClickableTrue(dialogButton);
                }else{
                    dialogButton.setTextColor(getResources().getColor(R.color.button_ok_color));
                    dialogButton.setBackground(getDrawable(R.drawable.custom_button));
                    viewVisibilityInvisible(cancelImageView);
                    viewClickableFalse(dialogButton);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        // if ok button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s=alert_video_bitrate.getText().toString();
                int ss=Integer.parseInt(s);
                if(ss<50||ss>5000){
                    s="1000";
                }
                bitrate_video_setting_text.setText(s+" kbps");
                saveVideoBitrate(s+ " kbps");
                v_bit_rate=Integer.parseInt(s);
                dialog.dismiss();
            }
        });

        // if cancel button is clicked, close the custom dialog
        dialogButtoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //camera screen orientation
        int ori=getWindowManager().getDefaultDisplay().getRotation();
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

        if(ori==0){
            wmlp.y = 350;
        }else{
            wmlp.y = 110;
        }
        dialog.show();
    }

    //video resolution to and from alert dialog
    private void videoResolutionAlert(int animationSource) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.resolution_video_alert,null);
        //setting font
        //calligrapher.setFont(view,"raleway_regular.ttf");
        builder.setView(view);
        final android.app.AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;

        final Button dialogButton = (Button) view.findViewById(R.id.alert_video_resolution_ok_id);
        Button dialogButtonCancel = (Button) view.findViewById(R.id.alert_video_resolution_cancel_id);
        resolutionRadioGroup = (RadioGroup) view.findViewById(R.id.resolution_view_group);

        for (int a=0;a<totalDisplayResolution;a++) {
            RadioButton rb = new RadioButton(this);
            rb.setId(a);
            //adding font family
            Typeface font = Typeface.createFromAsset(getAssets(), "raleway_regular.ttf");
            rb.setTypeface(font);
            //Log.d("TAG", "videoResolutionAlert: id: "+a);
            rb.setText(resolution[a]);
            if(resolution[a].equals(resolution_video_setting_text.getText().toString())){
                rb.setChecked(true);
            }
            Log.d("TAG", "videoResolutionAlert: getTextSize: "+rb.getTextSize());
            resolutionRadioGroup.addView(rb);
        }

        // if ok button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id=resolutionRadioGroup.getCheckedRadioButtonId();

                resolution_video_setting_text.setText(resolution[id]);
                saveVideoResolution(resolution[id]);

                //finding height and width separator
                final String videoResolution=resolution_video_setting_text.getText().toString().trim();
                Log.d("TAG", "videoResolution: "+videoResolution);
                int possi=0;
                for(int i=videoResolution.length()-1;i>=0;i--){
                    char a=videoResolution.charAt(i);
                    String a_s=String.valueOf(a);
                    if(a_s.equals("X")){
                        possi=i;
                        break;
                    }
                }

                v_res_height=Integer.parseInt(videoResolution.substring(0,possi));
                v_res_width=Integer.parseInt(videoResolution.substring(possi+1,videoResolution.length()));
                dialog.dismiss();
            }
        });

        // if cancel button is clicked, close the custom dialog
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //camera screen orientation
        int ori=getWindowManager().getDefaultDisplay().getRotation();
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

        if(ori==0){
            wmlp.y = 350;
        }else{
            wmlp.y = 110;
        }
        dialog.show();
    }

    //video framerate to and from alert dialog
    private void videoFrameAlert(int animationSource) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.framerate_video_alert,null);
        //setting font
        //calligrapher.setFont(view,"raleway_regular.ttf");
        builder.setView(view);
        final android.app.AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;

        final Button dialogButton = (Button) view.findViewById(R.id.alert_frame_video_ok_id);
        dialogButton.setTextColor(getResources().getColor(R.color.white));
        Button dialogButtonCancel = (Button) view.findViewById(R.id.alert_frame_video_cancel_id);
        final ImageView cancelImageView= (ImageView) view.findViewById(R.id.btn_clear_keyId);
        final RelativeLayout cancelImageView_key= (RelativeLayout) view.findViewById(R.id.btn_clear_lay_keyId);

        alert_video_framerate=view.findViewById(R.id.alert_framerate_video_id);
        alert_video_framerate.setHint("25");

        dialogButton.setBackground(getDrawable(R.drawable.custom_button_aoo_color));
        viewVisibilityVisible(cancelImageView);
        viewClickableTrue(dialogButton);
        cancelImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert_video_framerate.setText("");
            }
        });

        cancelImageView_key.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert_video_framerate.setText("");
            }
        });

        alert_video_framerate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(alert_video_framerate.getText().toString().trim().length()>0) {
                    dialogButton.setTextColor(getResources().getColor(R.color.white));
                    dialogButton.setBackground(getDrawable(R.drawable.custom_button_aoo_color));
                    viewVisibilityVisible(cancelImageView);
                    viewClickableTrue(dialogButton);
                }else{
                    dialogButton.setTextColor(getResources().getColor(R.color.button_ok_color));
                    dialogButton.setBackground(getDrawable(R.drawable.custom_button));
                    viewVisibilityInvisible(cancelImageView);
                    viewClickableFalse(dialogButton);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        alert_video_framerate.setText(framerate_video_setting_text.getText().toString().replace("fps","").trim());

        // if ok button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s=alert_video_framerate.getText().toString();

                int ss=Integer.parseInt(s);
                if(ss<15||ss>30){
                    s="30";
                }

                framerate_video_setting_text.setText(s+" fps");
                saveVideoFramerate(s+" fps");
                v_frame_rate=Integer.parseInt(s);
                dialog.dismiss();
            }
        });

        // if cancel button is clicked, close the custom dialog
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //camera screen orientation
        int ori=getWindowManager().getDefaultDisplay().getRotation();
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

        if(ori==0){
            wmlp.y = 350;
        }else{
            wmlp.y = 110;
        }
        dialog.show();
    }

    //audio samplerate to and from alert dialog
    private void audioSampleAlert(int animationSource) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.samplerate_audio_alert,null);
        //setting font
        //calligrapher.setFont(view,"raleway_regular.ttf");
        builder.setView(view);
        final android.app.AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;

        Button dialogButton = (Button) view.findViewById(R.id.alert_samplerate_audio_ok_id);
        Button dialogButtonCancel = (Button) view.findViewById(R.id.alert_samplerate_audio_cancel_id);

        sample_radio_1 = (RadioButton) view.findViewById(R.id.radio_id_1);
        sample_radio_2 = (RadioButton) view.findViewById(R.id.radio_id_2);
        sample_radio_3 = (RadioButton) view.findViewById(R.id.radio_id_3);
        sample_radio_4 = (RadioButton) view.findViewById(R.id.radio_id_4);
        sample_radio_5 = (RadioButton) view.findViewById(R.id.radio_id_5);

        if(samplerate_audio_setting_text.getText().toString().equals("11.025 kHz")) {
            sample_radio_1.setChecked(true);
        }else if(samplerate_audio_setting_text.getText().toString().equals("16.0 kHz")) {
            sample_radio_2.setChecked(true);
        }else if(samplerate_audio_setting_text.getText().toString().equals("22.05 kHz")) {
            sample_radio_3.setChecked(true);
        } else if(samplerate_audio_setting_text.getText().toString().equals("32.0 kHz")) {
            sample_radio_4.setChecked(true);
        }else if(samplerate_audio_setting_text.getText().toString().equals("44.1 kHz")) {
            sample_radio_5.setChecked(true);
        }

         //if ok button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sample_radio_1.isChecked()) {
                    samplerateSelected = sample_radio_1.getText().toString();
                } else if (sample_radio_2.isChecked()) {
                    samplerateSelected = sample_radio_2.getText().toString();
                } else if (sample_radio_3.isChecked()) {
                    samplerateSelected = sample_radio_3.getText().toString();
                } else if (sample_radio_4.isChecked()) {
                    samplerateSelected = sample_radio_4.getText().toString();
                } else if (sample_radio_5.isChecked()) {
                    samplerateSelected = sample_radio_5.getText().toString();
                }

                int srate=(Integer.parseInt(samplerateSelected));
                double ss=(double) srate/1000;
                a_sample_rate=srate;
                samplerate_audio_setting_text.setText(String.valueOf(ss)+" kHz");
                saveAudioSamplerate(String.valueOf(ss)+ " kHz");
                dialog.dismiss();
            }
        });

        //if cancel button is clicked, close the custom dialog
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //camera screen orientation
        int ori=getWindowManager().getDefaultDisplay().getRotation();
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

        if(ori==0){
            wmlp.y = 350;
        }else{
            wmlp.y = 110;
        }
        dialog.show();
    }

    //audio channel to and from alert dialog
    private void audioChannelAlert(int animationSource) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.channel_audio_alert,null);
        //setting font
        //calligrapher.setFont(view,"raleway_regular.ttf");
        builder.setView(view);
        final android.app.AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;

        Button dialogButton = (Button) view.findViewById(R.id.alert_channel_audio_ok_id);
        Button dialogButtonCancel = (Button) view.findViewById(R.id.alert_channel_audio_cancel_id);

        channel_radio_1 = (RadioButton) view.findViewById(R.id.radio_id_mono);
        channel_radio_2 = (RadioButton) view.findViewById(R.id.radio_id_stereo);

        if(channel_audio_setting_text.getText().toString().equals("Mono")) {
            channel_radio_1.setChecked(true);
        }else{
            channel_radio_2.setChecked(true);
        }

        //if ok button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (channel_radio_1.isChecked()) {
                    channelSelected = channel_radio_1.getText().toString();
                    isStereo=false;
                } else if (channel_radio_2.isChecked()) {
                    channelSelected = channel_radio_2.getText().toString();
                    isStereo=true;
                }

                channel_audio_setting_text.setText(channelSelected);
                saveAudioChannel(channelSelected);
                dialog.dismiss();
            }
        });

        //if cancel button is clicked, close the custom dialog
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //camera screen orientation
        int ori=getWindowManager().getDefaultDisplay().getRotation();
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

        if(ori==0){
            wmlp.y = 350;
        }else{
            wmlp.y = 110;
        }
        dialog.show();
    }

    //camera mode alert dialog
    private void cameraModeAlert(int animationSource) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.angle_camera_alert,null);
        //setting font
        //calligrapher.setFont(view,"raleway_regular.ttf");
        builder.setView(view);
        final android.app.AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;


        Button dialogButton = (Button) view.findViewById(R.id.alert_angle_ok_id);
        Button dialogButtonCancel = (Button) view.findViewById(R.id.alert_angle_cancel_id);

        radio_mode_portrait = (RadioButton) view.findViewById(R.id.radio_id_portrait);
        radio_mode_landscape = (RadioButton) view.findViewById(R.id.radio_id_landscape);
        radio_mode_landscape_reverse = (RadioButton) view.findViewById(R.id.radio_id_landscape_reverse);

        if(camera_mode_setting_text.getText().toString().equals("Portrait")) {
            radio_mode_portrait.setChecked(true);
        }else if(camera_mode_setting_text.getText().toString().equals("Landscape")){
            radio_mode_landscape.setChecked(true);
        }else{
            radio_mode_landscape_reverse.setChecked(true);
        }

        //if ok button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radio_mode_portrait.isChecked()) {
                    saveSelectedCameraMode("Portrait");
                    cameraModeSelected = radio_mode_portrait.getText().toString();
                } else if (radio_mode_landscape.isChecked()) {
                    saveSelectedCameraMode("Landscape");
                    cameraModeSelected = radio_mode_landscape.getText().toString();
                }else if(radio_mode_landscape_reverse.isChecked()) {
                    saveSelectedCameraMode("Reverse Landscape");
                    cameraModeSelected = radio_mode_landscape_reverse.getText().toString();
                }

                camera_mode_setting_text.setText(cameraModeSelected);
                dialog.dismiss();
            }
        });

        //if cancel button is clicked, close the custom dialog
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //camera screen orientation
        int ori=getWindowManager().getDefaultDisplay().getRotation();

        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        //wmlp.x = 100;   //x position
        if(ori==0){
            wmlp.y = 350;   //y position
        }else{
            wmlp.y = 110;   //y position
        }
        dialog.show();
    }

    //recording option to and from alert dialog
    private void recordingAlert(int animationSource) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.stream_record,null);
        //setting font
        //calligrapher.setFont(view,"raleway_regular.ttf");
        builder.setView(view);
        final android.app.AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;

        Button dialogButton = (Button) view.findViewById(R.id.alert_record_ok_id);
        Button dialogButtonCancel = (Button) view.findViewById(R.id.alert_record_cancel_id);

        record_radio_1 = (RadioButton) view.findViewById(R.id.radio_id_rec_enable);
        record_radio_2 = (RadioButton) view.findViewById(R.id.radio_id_rec_disable);

        if(recording_setting_text.getText().toString().equals("Enabled")) {
            record_radio_1.setChecked(true);
        }else{
            record_radio_2.setChecked(true);
        }

        //if ok button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (record_radio_1.isChecked()) {
                    recordSelected = record_radio_1.getText().toString();
                    willRecord=true;
                } else if (record_radio_2.isChecked()) {
                    recordSelected = record_radio_2.getText().toString();
                    willRecord=false;
                }

                recording_setting_text.setText(recordSelected);
                saveRecording(recordSelected);
                dialog.dismiss();
            }
        });

        //if cancel button is clicked, close the custom dialog
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //camera screen orientation
        int ori=getWindowManager().getDefaultDisplay().getRotation();

        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        //wmlp.x = 100;   //x position
        if(ori==0){
            wmlp.y = 350;   //y position
        }else{
            wmlp.y = 110;   //y position
        }
        dialog.show();
    }

    //audio bit to and from alert dialog
    private void audioBitAlert(int animationSource) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.bitrate_audio_alert,null);
        //setting font
        //calligrapher.setFont(view,"raleway_regular.ttf");
        builder.setView(view);
        final android.app.AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;

        final Button dialogButton = (Button) view.findViewById(R.id.alert_bit_audio_ok_id);
        dialogButton.setTextColor(getResources().getColor(R.color.white));
        Button dialogButtonCancel = (Button) view.findViewById(R.id.alert_bit_audio_cancel_id);
        final ImageView cancelImageView= (ImageView) view.findViewById(R.id.btn_clear_keyId);
        final RelativeLayout cancelImageView_key= (RelativeLayout) view.findViewById(R.id.btn_clear_lay_keyId);

        alert_audio_bitrate=(EditText) view.findViewById(R.id.alert_bit_audio_id);

        alert_audio_bitrate.setText(bitrate_audio_setting_text.getText().toString().replace("kbps","").trim());
        alert_audio_bitrate.setHint("128");

        cancelImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert_audio_bitrate.setText("");
            }
        });

        cancelImageView_key.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert_audio_bitrate.setText("");
            }
        });
        dialogButton.setBackground(getDrawable(R.drawable.custom_button_aoo_color));
        viewVisibilityVisible(cancelImageView);
        viewClickableTrue(dialogButton);

        alert_audio_bitrate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(alert_audio_bitrate.getText().toString().trim().length()>0) {
                    dialogButton.setTextColor(getResources().getColor(R.color.white));
                    dialogButton.setBackground(getDrawable(R.drawable.custom_button_aoo_color));
                    viewVisibilityVisible(cancelImageView);
                    viewClickableTrue(dialogButton);
                }else{
                    dialogButton.setTextColor(getResources().getColor(R.color.button_ok_color));
                    dialogButton.setBackground(getDrawable(R.drawable.custom_button));
                    viewVisibilityInvisible(cancelImageView);
                    viewClickableFalse(dialogButton);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        // if ok button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String s=alert_audio_bitrate.getText().toString();

                int ss=Integer.parseInt(s);
                if(ss<32||ss>512){
                    s="128";
                }

                a_bit_rate=Integer.parseInt(s);
                bitrate_audio_setting_text.setText(s+" kbps");
                saveAudioBitrate(s+ " kbps");
                dialog.dismiss();
            }
        });

        // if cancel button is clicked, close the custom dialog
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //camera screen orientation
        int ori=getWindowManager().getDefaultDisplay().getRotation();

        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        //wmlp.x = 100;   //x position
        if(ori==0){
            wmlp.y = 350;   //y position
        }else{
            wmlp.y = 110;   //y position
        }
        dialog.show();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    //makes video quality..
    //called when clicked camera live button
    private boolean prepareEncoders() {
        Log.d("TAG", "prepareEncoders: sample rate: "+isStereo);
        //height & width interchanged because mobile device's height & width is reversed from another device
        return rtmpCamera1.prepareVideo(/*width*/v_res_height,/* height*/v_res_width,
                /*Integer*/ v_frame_rate,/*Integer*/ v_bit_rate* 1024,
                /*cbHardwareRotation.isChecked()*/ false,
                CameraHelper.getCameraOrientation(this))
                && rtmpCamera1.prepareAudio(/*Integer*/a_bit_rate * 1024,
                /*Integer*/a_sample_rate,/*boolean*/isStereo, /*Integer*/echoChanceler,
                /*Integer*/noiseSuppressor);
    }

    //when successfully connected with RTMP protocol
    @Override
    public void onConnectionSuccessRtmp() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startCountingLive();
                if(recording_setting_text.getText().toString().equals("Enabled")){
                    try {
                        rtmpCamera1.startRecord(folder.getAbsolutePath() + "/" + currentDateAndTime + ".mp4");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    //when failed to connect with RTMP protocol
    @Override
    public void onConnectionFailedRtmp(final String reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(RtmpActivity.this,"Error: Check your RTMP URL",Toast.LENGTH_LONG).show();
                stopCountingLive();
                rtmpCamera1.stopStream();
                streaming=false;
                streamOnOffIcon.setImageResource(R.drawable.synopi_live_video_white);
                rtmpCamera1.stopRecord();
                currentDateAndTime = "";
            }
        });
    }

    //when disconnected from RTMP protocol
    @Override
    public void onDisconnectRtmp() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                stopCountingLive();
                rtmpCamera1.stopStream();
                streaming=false;
                streamOnOffIcon.setImageResource(R.drawable.synopi_live_video_white);
                rtmpCamera1.stopRecord();
                currentDateAndTime = "";
            }
        });
    }

    //unsuccessful user authentication
    @Override
    public void onAuthErrorRtmp() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(RtmpActivity.this, "Auth error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //successful user authentication
    @Override
    public void onAuthSuccessRtmp() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(RtmpActivity.this, "Auth success", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //surface holder listener(surfaceCreated)
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) { }

    //surface holder listener(surfaceChanged)
    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
            rtmpCamera1.startPreview();
    }

    //surface holder listener(surfaceDestroyed)
    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (rtmpCamera1.isStreaming()) {
            rtmpCamera1.stopStream();
            rtmpCamera1.stopRecord();

            currentDateAndTime = "";
            streamOnOffIcon.setImageResource(R.drawable.synopi_live_video_white);
            streaming=false;
        }
        rtmpCamera1.stopPreview();
    }

    //this method called for zooming and display single touch event
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (motionEvent.getPointerCount() > 1) {
            if (action == MotionEvent.ACTION_MOVE) {
                rtmpCamera1.setZoom(motionEvent);
            }
        } else {
            if (action == MotionEvent.ACTION_UP) {

            }
        }
        return true;
    }

    //startCountingLive enabled to calculate Streaming time
    void startCountingLive() {
        streamOnOffIcon.setImageResource(R.drawable.synopi_live_video_white_red);
        streaming=true;
        Log.d("Tag","mainActivity....check startCountingLive: ");
        StartTime = SystemClock.uptimeMillis();
        handler.postDelayed(runnable, 0);
    }

    //stopCountingLive disabled to calculate Streaming time
    private void stopCountingLive() {
        streamOnOffIcon.setImageResource(R.drawable.synopi_live_video_white);
        streaming=false;
        Log.d("Tag","mainActivity....check stopCountingLive: ");
        label.setText("00:00:00");
        viewVisibilityGone(label);
        viewVisibilityInvisible(redIconImage);
        settingControl.setImageResource(R.drawable.settings_icon);
        viewClickableTrue(settingControl);
        handler.removeCallbacks(runnable);
    }

    //for calculating streaming time
    public Runnable runnable = new Runnable() {
        public void run() {
            MillisecondTime = SystemClock.uptimeMillis() - StartTime;
            UpdateTime = TimeBuff + MillisecondTime;
            Seconds = (int) (UpdateTime / 1000);
            Hours=Minutes/60;
            Minutes = Seconds / 60;
            Seconds = Seconds % 60;
            MilliSeconds = (int) (UpdateTime % 1000);
            if(rtmpCamera1.isStreaming()){
                streamOnOffIcon.setImageResource(R.drawable.synopi_live_video_white_red);
                label.setText(String.format("%02d", Hours)+ ":" + String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds));
                viewVisibilityVisible(label);
            }

            if (Seconds%2==0&&rtmpCamera1.isStreaming()){
                viewVisibilityVisible(redIconImage);
            }else{
                viewVisibilityInvisible(redIconImage);
            }

            handler.postDelayed(this, 0);
        }
    };

    //clicked back button
    @Override
    public void onBackPressed() {

        if(streaming){
            alertLiveStreaming("Live Streaming");
        }else if(urlRelativeLayout.getVisibility()==View.VISIBLE){
            setOrientation();
            viewVisibilityGone(urlRelativeLayout);
            viewVisibilityVisible(streamOnOffIcon);
            viewClickableTrue(microphoneControl,cameraControl,switchCamera,streamOnOffIcon);

            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);
        }else{

            if (doubleBackToExitPressedOnce) {
                finishAffinity();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
            //overridePendingTransition(R.anim.slide_activity_in_left,R.anim.slide_activity_out_right);
        }

    }

    //clicked setting icon when app is in onStream
    private void alertLiveStreaming(String stream_record) {
        String streamRecord=stream_record;

        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(RtmpActivity.this);
        alertDialog2.setTitle("Are you want to Exit?");
        alertDialog2.setMessage("Exit will make your stream Disconnected");

        alertDialog2.setPositiveButton("Disconnect",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        rtmpCamera1.stopRecord();
                        currentDateAndTime = "";
                        rtmpCamera1.stopStream();
                        stopCountingLive();
                        streamOnOffIcon.setImageResource(R.drawable.synopi_live_video_white);
                        streaming=false;
                        onBackPressed();
                    }
                });

        alertDialog2.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog2.show();
    }

    //onResume checked user permission again
    @Override
    protected void onResume() {
        super.onResume();
    }

    /*
    when app goes to pause state, then streaming & recording goes stop
     */
    @Override
    protected void onPause() {
        rtmpCamera1.stopRecord();
        currentDateAndTime = "";
        rtmpCamera1.stopStream();
        stopCountingLive();
        streamOnOffIcon.setImageResource(R.drawable.synopi_live_video_white);
        streaming=false;
        super.onPause();
    }

    /*
    when app goes to stop state, then streaming & recording goes stop
     */
    @Override
    protected void onStop() {
        rtmpCamera1.stopRecord();
        currentDateAndTime = "";
        rtmpCamera1.stopStream();
        stopCountingLive();
        streamOnOffIcon.setImageResource(R.drawable.synopi_live_video_white);
        streaming=false;
        super.onStop();
    }

    //checking internet connection
    public boolean isConnected() {
        boolean connected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
            return connected;
        } catch (Exception e) {
            Log.e("Connectivity Exception", e.getMessage());
        }
        return connected;
    }

    //current network status
    public static void dialog(boolean value){
        //Log.d("TAG", "snackView.getHeight: " );
        if (value) {
            if(connectionFromFirstActivity.equals("1")) {

            }else {
                if(isOffline) {
                    snackbar.setDuration(Snackbar.LENGTH_SHORT);
                    textView.setText("Back to Online");
                    snackView.setBackgroundColor(Color.parseColor("#158A5F"));
                    textView.setTextColor(Color.WHITE);
                    textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    isOffline=false;
                    snackbar.show();
                }
            }
        } else {
            connectionFromFirstActivity="0";
            snackbar.setDuration(Snackbar.LENGTH_INDEFINITE);
            snackView.setBackgroundColor(Color.parseColor("#514E4E"));
            textView.setText("No Internet Connection");
            isOffline=true;
            textView.setTextColor(Color.WHITE);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            snackbar.show();
        }
    }

    //current network status
    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    //current network status
    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    //used for visible of views
    private void viewVisibilityVisible(View... views) {
        for (View v : views) {
            v.setVisibility(View.VISIBLE);
        }
    }

    //used for invisible of views
    private void viewVisibilityInvisible(View... views) {
        for (View v : views) {
            v.setVisibility(View.INVISIBLE);
        }
    }

    //used for visibility gone of views
    private void viewVisibilityGone(View... views) {
        for (View v : views) {
            v.setVisibility(View.GONE);
        }
    }

    //used for clickable true of views
    private void viewClickableTrue(View... views) {
        for (View v : views) {
            v.setClickable(true);
        }
    }

    //used for clickable false of views
    private void viewClickableFalse(View... views) {
        for (View v : views) {
            v.setClickable(false);
        }
    }
}
