package com.synopi.live.synopirtmplive360;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

class Crypto {

    private SecretKeySpec skc;


    Crypto(String token) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(("YourStrongKey" + token).getBytes("UTF-8"));
            skc = new SecretKeySpec(thedigest, "AES/ECB/PKCS5Padding");

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public byte[] encrypt(String clear) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, skc);
        byte[] input = clear.getBytes("utf-8");
        byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
        int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
        cipher.doFinal(cipherText, ctLength);
        return cipherText;
    }

    public byte[] decrypt(byte[] buf, int offset, int len) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, skc);
        byte[] dec = new byte[cipher.getOutputSize(len - offset)];
        int ctLength = cipher.update(buf, offset, len - offset, dec, 0);
        cipher.doFinal(dec, ctLength);
        return dec;
    }
}

    //String GET_URL = "http://61.247.183.178:8002/resolve/46ce41fe30c5d119b58bb184f1a2816b409c30e1a124dc8f9729ceaeb70ff5de978839d65f0762f3a9994509b564d681/eqvwJ-6wl68:APA91bGD55dQBEwrRPenmeJW7Z5A5mM6YA-r_UwJB3jYfkCeBpnAeGnL-9R7TL4hr9v3d_fmJ-8lA8S4AeK11ouggW6l6Ipk7cbe96weE1gQj9i_35BcCZ4rETAOZG-2NCkGM_3WkLhw";
    //String GET_URL = "http://192.168.207.24/url_1.php?";
    //int GET_URL_REQUEST_CODE = 1;